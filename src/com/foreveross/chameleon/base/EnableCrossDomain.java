package com.foreveross.chameleon.base;

import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.webkit.WebView;
import com.foreveross.chameleon.utils.AppLog;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @author: wzq
 * @date: 14-6-11
 * description: change it at File | setting | File and code templates | include | file header
 */
public class EnableCrossDomain {
    private static final String TAG = EnableCrossDomain.class.getSimpleName();

    // TODO 处理跨域问题enable cross domin
    public static void enableContentAsLocal(WebView webview) {
        // 4.4 - 4.1 - 4.0
        if (VERSION.SDK_INT >= 19) {//VERSION_CODES.KitKat 4.4.2
            enablecrossdomain4_4(webview);
        } else if (VERSION.SDK_INT <= VERSION_CODES.ICE_CREAM_SANDWICH_MR1) {
            enablecrossdomain4_0(webview);
        } else {
            enablecrossdomain4_1(webview);
        }
    }

    private static void enablecrossdomain4_0(WebView webview) {
        try {
            Field field = WebView.class.getDeclaredField("mWebViewCore");
            field.setAccessible(true);
            Object webviewcore = field.get(webview);
            Method method = webviewcore.getClass().getDeclaredMethod("nativeRegisterURLSchemeAsLocal", String.class);
            method.setAccessible(true);
            method.invoke(webviewcore, "content");
            AppLog.i("enablecrossdomain", "success");
        } catch (Exception e) {
            AppLog.e(TAG, "enablecrossdomain error");
            e.printStackTrace();
            AppLog.e("enablecrossdomain", "fail");
        }
    }

    private static void enablecrossdomain4_1(WebView webView) {
        try {
            Method method;
            method = webView.getSettings().getClass().getMethod("setAllowUniversalAccessFromFileURLs", boolean.class);
            if (method != null) {
                method.invoke(webView.getSettings(), true);
            }
        } catch (NoSuchMethodException e) {
            AppLog.d(TAG, "setAllowUniversalAccessFromFileURLs() for webview failed");
        } catch (IllegalArgumentException e) {
            AppLog.e(TAG, "setAllowUniversalAccessFromFileURLs() for webview failed");
        } catch (IllegalAccessException e) {
            AppLog.e(TAG, "setAllowUniversalAccessFromFileURLs() for webview failed");
        } catch (InvocationTargetException e) {
            AppLog.e(TAG, "setAllowUniversalAccessFromFileURLs() for webview failed");
        }
        //
        try {
            Method method = webView.getClass().getMethod("getWebViewProvider");
            if (method != null) {
                Object provider = method.invoke(webView);
                method = provider != null ? provider.getClass().getMethod("getWebViewCore") : null;
                if (method != null) {
                    Object webViewCore = method.invoke(provider);
                    method = webViewCore != null ? webViewCore.getClass()
                            .getDeclaredMethod("nativeRegisterURLSchemeAsLocal", int.class, String.class) : null;
                    if (method != null) {
                        Field field = webViewCore.getClass().getDeclaredField("mNativeClass");
                        if (field != null) {
                            field.setAccessible(true);
                            method.setAccessible(true);
                            method.invoke(webViewCore, field.get(webViewCore), "content");
                        }
                    }
                }
            }
        } catch (NoSuchMethodException e) {
            AppLog.w(TAG, "nativeRegisterURLSchemeAsLocal() for webview failed. " + e);
        } catch (IllegalArgumentException e) {
            AppLog.w(TAG, "nativeRegisterURLSchemeAsLocal() for webview failed. " + e);
        } catch (IllegalAccessException e) {
            AppLog.w(TAG, "nativeRegisterURLSchemeAsLocal() for webview failed. " + e);
        } catch (InvocationTargetException e) {
            AppLog.w(TAG, "nativeRegisterURLSchemeAsLocal() for webview failed. " + e);
        } catch (NoSuchFieldException e) {
            AppLog.w(TAG, "nativeRegisterURLSchemeAsLocal() for webview failed. " + e);
        }
        //        webView.getSettings().setAllowUniversalAccessFromFileURLs(true);
        //        try {
        //            Field webviewclassic_field = WebView.class.getDeclaredField("mProvider");
        //            webviewclassic_field.setAccessible(true);
        //            Object webviewclassic = webviewclassic_field.get(webView);
        //            Field webviewcore_field = webviewclassic.getClass().getDeclaredField("mWebViewCore");
        //            webviewcore_field.setAccessible(true);
        //            Object mWebViewCore = webviewcore_field.get(webviewclassic);
        //            Field nativeclass_field = webviewclassic.getClass().getDeclaredField("mNativeClass");
        //            nativeclass_field.setAccessible(true);
        //            Object mNativeClass = nativeclass_field.get(webviewclassic);
        //            Method method = mWebViewCore.getClass()
        //                    .getDeclaredMethod("nativeRegisterURLSchemeAsLocal", new Class[]{int.class, String.class});
        //            method.setAccessible(true);
        //            method.invoke(mWebViewCore, "content");
        //            AppLog.i("enablecrossdomain", "success");
        //        } catch(Exception e) {
        //            AppLog.e(TAG, "enablecrossdomain error");
        //            e.printStackTrace();
        //            AppLog.e("enablecrossdomain", "fail");
        //        }
    }

    //private static void enablecrossdomain4_2(WebView webView) {}
    //private static void enablecrossdomain4_3(WebView webView) {}
    private static void enablecrossdomain4_4(WebView webView) {
        webView.getSettings().setAllowUniversalAccessFromFileURLs(true);
        try {
            Field webviewclassic_field = WebView.class.getDeclaredField("mProvider");
            webviewclassic_field.setAccessible(true);
            Object webviewclassic = webviewclassic_field.get(webView);


            Field webviewcore_field = webviewclassic.getClass().getDeclaredField("mWebViewCore");
            webviewcore_field.setAccessible(true);
            Object mWebViewCore = webviewcore_field.get(webviewclassic);
            Field nativeclass_field = webviewclassic.getClass().getDeclaredField("mNativeClass");
            nativeclass_field.setAccessible(true);
            Object mNativeClass = nativeclass_field.get(webviewclassic);
            Method method = mWebViewCore.getClass()
                    .getDeclaredMethod("nativeRegisterURLSchemeAsLocal", new Class[]{int.class, String.class});
            method.setAccessible(true);
            method.invoke(mWebViewCore, "content");
            AppLog.i("enablecrossdomain", "success");
        } catch (Exception e) {
            //AppLog.e(TAG, "enablecrossdomain error");
            //e.printStackTrace();
            AppLog.e("enablecrossdomain", "fail");
        }
    }
}
