package com.foreveross.chameleon.base;

import android.app.Application;

import com.foreveross.chameleon.utils.Preferences;

public class PushApplication extends Application{

	public final static String TAG = "PushApplication";

	public static PushApplication mInstance = null;

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		mInstance = this;
        init();
	}

    private static void init() {
        Preferences.getInstance(getApplication());
        //URL.init(getApplication());
    }
	public static PushApplication getApplication() {
        if(mInstance == null) {
            throw new RuntimeException("必须继承PushApp");
        }
		return mInstance;
	}
}
