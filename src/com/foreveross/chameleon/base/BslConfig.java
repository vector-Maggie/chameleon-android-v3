package com.foreveross.chameleon.base;

import android.os.Environment;
import com.foreveross.chameleon.exhibition.BuildConfig;
import com.foreveross.chameleon.exhibition.R;
import com.foreveross.chameleon.utils.AndroidTool;

/**
 * 配置文件，常量
 */
public class BslConfig {
    static {
        String mod = BslApplication.getInstance().getString(R.string.build_config_sandbox);
        IS_SANDBOX = "sandbox".equals(mod);

        // 判断sd卡是否存在
        if ("sandbox".equals(mod)) {
            String state = Environment.getExternalStorageState();
            if (!Environment.MEDIA_MOUNTED.equals(state)) {
                throw new RuntimeException("sandbox模式，但是sd卡不可用");
            }
            EXT_STORE = BslApplication.getInstance().getExternalFilesDir(null).getAbsolutePath();
        } else {
            EXT_STORE = null;
        }
    }

    // these values must be init first
    public static final boolean IS_SANDBOX;
    public static final String EXT_STORE;

    // direct definition
    // public static final String UTF8 = "UTF-8";
    // public static final String PKG_NAME = AndroidTool.getPkgName();
    // private static final String SD_CARD = Environment.getExternalStorageDirectory().getAbsolutePath();
    public static final String AES_PASSWORD = BuildConfig.AES_PASSWORD;

    // defined by called other const
    // public static final String APP_FOLDER_IN_SD = EXT_STORE + "/" + AndroidTool.getPkgName();
    public static final String WWW_FOLDER_IN_SD = EXT_STORE + "/www";
    public static final String ASSETS_FOLDER_AFTER_SCHEME = "/file:///android_asset/";
    public static final String ASSETS_WWW_FOLDER = "file:///android_asset/www";
    public static final String FILE_SCHEME = "file://";
    // key for sth
    public static final String SP_KEY_APPVERSION = "appVersion";
    public static final String SP_KEY_FIRST_INSTALL = "firstInstall";

    // provider的authorities 从xml中读取
    public static final String AUTH_PROVIDER_CRYPT = BslApplication.getInstance().getString(R.string.providerAuth);
    public static final String SCHEME_PROVIDER = "content://" + AUTH_PROVIDER_CRYPT;
    //public static final String AUTH_PROVIDER_CRYPT = "com.foss.chameleon.provider.crypt";
    //public static final String SCHEME_PROVIDER = "content://com.foss.chameleon.provider.crypt";

    // sharepreference key, value=false(default)/true
    public static final String FLAG_FILE_COPYED = "file_alrady_copy_to_sd";

    public static final boolean IS_CRYPT = false;
    public static final boolean ENABLE_MDM = false;
}
