package com.foreveross.chameleon.base;

import android.app.Application;

/**
 * Created by wzq on 14-8-19.
 */
public class BslApplication extends PushApplication {
    private static Application instance;

    public static Application getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    /**
     * 程序启动初始化
     */
    public void initAtAppStart() {
        // TODO
    }
}
