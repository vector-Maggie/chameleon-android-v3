package com.foreveross.chameleon.socket;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.foreveross.chameleon.utils.GeneralUtils;


public class ReceiverNetwork extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        boolean isNetAvail = GeneralUtils.isNetworkConnected();
        //AppLog.i("Network is changed, avail = " + isNetAvail);
         if (isNetAvail) {
             context.startService(new Intent(context, ServicePush.class));
         } else {
             context.stopService(new Intent(context, ServicePush.class));
         }
    }
}
