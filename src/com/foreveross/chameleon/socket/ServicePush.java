package com.foreveross.chameleon.socket;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import com.foreveross.chameleon.utils.AppLog;

/**
 * @author 黄黎
 * @version 创建时间：2014年5月12日 下午3:46:25
 *          类说明
 */
public class ServicePush extends Service {
    public final static String TAG = "ServicePush";
    private ReceiverUserChanged userChangeReceiver;

    @Override
    public void onCreate() {
        super.onCreate();
        //AppLog.i("Push Service is create");
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //AppLog.i("Push Service is onStartCommand");
        // TODO 如果服务未启动，则启动，否则不用启动

        /*if(userChangeReceiver == null) {
            userChangeReceiver = new ReceiverUserChanged();
            IntentFilter userChangeFilter = new IntentFilter(BroadcastConstans.SecurityRefreshMainPage);
            registerReceiver(userChangeReceiver, userChangeFilter);
        }*/
        AppLog.d(TAG, "启动push服务!");
        PushSetup.getInstance().startSetup("启动Push服务", true, false);
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        //AppLog.i("Push Service is onBind");
        return null;
    }

    @Override
    public void onDestroy() {
        //AppLog.i("Push Service is onDestroy");
        AppLog.d(TAG,"推送服务被关闭");
        PushSetup.getInstance().stopSetup(true);
        if(userChangeReceiver != null) {
            unregisterReceiver(userChangeReceiver);
            userChangeReceiver = null;
        }
        super.onDestroy();
    }

    // 用户切换时的广播
    public class ReceiverUserChanged extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            //AppLog.i("用户切换，重新连接push服务器");
            PushSetup.getInstance().startSetup("用户切换，重新连接push服务器", false, true);
        }
    }
}
