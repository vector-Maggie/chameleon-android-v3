package com.foreveross.chameleon.socket;

import org.msgpack.annotation.Message;

/**
 * @author 黄黎
 * @version 创建时间：2014年5月19日 下午1:56:48
 * @description 申请token时 上送的信息
 */
@Message
public class AuthMessageEntity {

    public String userName;
    public String pushToken;
    public String deviceId;

    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }
    public String getPushToken() {
        return pushToken;
    }
    public void setPushToken(String pushToken) {
        this.pushToken = pushToken;
    }
    public String getDeviceId() {
        return deviceId;
    }
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

}
