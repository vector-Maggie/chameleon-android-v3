package com.foreveross.chameleon.sandbox;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;

/**
 * @project: chameleon-android-v3
 * @date: 14-8-27
 * @author: wzq
 * description: TODO
 */
public class CryptPrintWriter extends Writer {
    private PrintWriter innerWriter;

    public CryptPrintWriter(Writer wr) {
        innerWriter = new PrintWriter(wr);
    }

    public void close() {
        innerWriter.close();
    }

    @Override
    public void flush() throws IOException {
        innerWriter.flush();
    }

    @Override
    public void write(char[] buf, int offset, int count) throws IOException {
        innerWriter.write(buf, offset, count);
    }

    public void println(String str) {
        innerWriter.println(str);
    }
}
