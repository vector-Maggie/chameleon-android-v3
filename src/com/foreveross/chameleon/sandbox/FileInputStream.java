package com.foreveross.chameleon.sandbox;

import com.foreveross.chameleon.utils.AesTool;
import com.foreveross.chameleon.utils.AppLog;
import com.foreveross.chameleon.utils.IOUtils;

import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * @project: chameleon-android-v3
 * @date: 14-8-27
 * @author: wzq
 * description: TODO
 */
public class FileInputStream extends java.io.InputStream {
    private java.io.InputStream innerStream;

    public FileInputStream(java.io.File f) throws IOException {
        java.io.InputStream srcStream = new java.io.FileInputStream(f);
        if (srcStream == null) {
            AppLog.e("null srcStream");
        }
        if (srcStream.available() == 0) {
            AppLog.e("0 srcStream");
        }
        byte[] b = IOUtils.stream2Bytes(srcStream);
        AppLog.i(IOUtils.printByteArray(b));
//        byte[] de = AesTool.getInstance().decryptBytes(b);
        byte[] de = AesTool.decryptBytes(b);
        innerStream = IOUtils.bytes2Stream(de);
        srcStream.close();
    }

    public void close() throws IOException {
        if (innerStream != null) {
            innerStream.close();
        }
    }

    @Override
    public int read() throws IOException {
        int size = -1; // do not modify
        if (innerStream != null) {
            size = innerStream.read();
        }
        return size;
    }
}
