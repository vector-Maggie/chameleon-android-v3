package com.foreveross.chameleon.sandbox;

import com.foreveross.chameleon.utils.AesTool;
import com.foreveross.chameleon.utils.AppLog;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * @author: wzq
 * @date: 14-7-28
 * description: 加解密插件 encrypt/decrypt
 */
public class PluginCrypt extends CordovaPlugin {
    private static final String ACTION_ENCRYPT = "encrypt";
    private static final String ACTION_DECRYPT = "decrypt";
    private static final String FILE_SRC = "src";
    private static final String FILE_DST = "dst";

    // string 加解密
    @Override
    public boolean execute(String action, String rawArgs, CallbackContext callbackContext) throws JSONException {
        AppLog.d("plugin Crypt String , action=" + action + ",rawArgs=" + rawArgs);

        // 判断传递进来的是否为JSONArray
        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray(rawArgs);
        } catch (JSONException e) {
            // 非json,无须处理
        }
        if (jsonArray != null) {
            return execute(action, jsonArray, callbackContext);
        } else {
            AppLog.d("字符串加密");
        }

        // encrypt
        if (ACTION_ENCRYPT.equals(action)) {
//            String str = AesTool.getInstance().encryptString(rawArgs);
            String str = AesTool.encryptString(rawArgs);
            if (null != str) {
                callbackContext.success(str);
            } else {
                callbackContext.error("加密失败");
            }
            return true;
        }

        // decrypt
        if (ACTION_DECRYPT.equals(action)) {
            // 取出"
            rawArgs = rawArgs.replace("\"", "");
//            String str = AesTool.getInstance().decryptString(rawArgs);
            String str = AesTool.decryptString(rawArgs);
            if (str != null) {
                callbackContext.success(str);
            } else {
                callbackContext.error("解密失败");
            }
            return true;
        }

        //
        AppLog.e("未定义的action = " + action);
        return false;
    }

    //文件加解密
    @Override
    public boolean execute(String action, JSONArray rawArgs, CallbackContext callbackContext) throws JSONException {
        AppLog.d("plugin Crypt File , action=" + action + ",rawArgs=" + rawArgs.toString());

        // 查找src 要被加解密的文件
        JSONObject srcInfo = (JSONObject) rawArgs.get(0);
        String src = srcInfo.getString(FILE_SRC);
        if (src == null) {
            AppLog.e("src fileUri invalid");
            callbackContext.error("src fileUri invalid");
            return false;
        }

        // 查找dst 代表加解密后,生成的文件位置
        String dst = src;
        if (rawArgs.length() > 1) {
            JSONObject dstInfo = (JSONObject) rawArgs.get(1);
            if (dstInfo.has(FILE_DST)) {
                dst = dstInfo.getString(FILE_DST);
            }
        }
        AppLog.d("src=" + src + ",dst=" + dst);
        // encrypt
        if (ACTION_ENCRYPT.equals(action)) {
            try {
//                AesTool.getInstance().encryptFile(src, dst);
                AesTool.encryptFile(src, dst);
                callbackContext.success();
            } catch (IOException e) {
                AppLog.e("加密文件失败");
                callbackContext.error("加密文件失败");
                e.printStackTrace();
            }
            return true;
        }

        // decrypt
        if (ACTION_DECRYPT.equals(action)) {
            try {
//                AesTool.getInstance().decryptFile(src, dst);
                AesTool.decryptFile(src, dst);
                callbackContext.success();
            } catch (IOException e) {
                AppLog.e("解密文件失败");
                callbackContext.error("解密文件失败");
                e.printStackTrace();
            }
            return true;
        }

        //
        AppLog.e("未定义的action = " + action);
        return false;
    }
}
