package com.foreveross.chameleon.mdm;
import com.foreveross.chameleon.utils.AppLog;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author: wzq
 * @date: 14-7-31
 * description: 数据模型1:从服务端拉取的 mdm 信息
 */
class ModelInfoFromServer {
    public static final String TAG = "ModelInfoFromServer";
    /** 数据模型1:从服务端拉取的 mdm 信息 */
    public static final int RESULT_OK = 0;
    public static final int RESULT_FAIL = 1;
    int code = -1;
    String cmdContent;

    /** 解析从服务端拉取的 mdm 数据 ，【code + preserved】 */
    public static ModelInfoFromServer parseCmd(String infoPulled) {
        if(infoPulled == null) {
            return null;
        }
        final String TAG_CODE = "code";
        final String TAG_PRESERVED = "preserved";
        ModelInfoFromServer mdmCommand = null;

        try {
            JSONObject json = new JSONObject(infoPulled);
            int jsonCode = json.getInt(TAG_CODE);
            String ctnt = json.getString(TAG_PRESERVED);
            AppLog.i(TAG, "preserved内容=\n" + ctnt);

            mdmCommand = new ModelInfoFromServer();
            mdmCommand.code = jsonCode;
            mdmCommand.cmdContent = ctnt;
        } catch(JSONException e) {
            e.printStackTrace();
        }
        return mdmCommand;
    }
}
