package com.foreveross.chameleon.mdm;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import com.foreveross.chameleon.utils.AppLog;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.json.JSONException;

/**
 * @author: wzq
 * @date: 14-7-31
 * description: 类说明
 */
public class PluginMdm extends CordovaPlugin {

    public final static String TAG = "PluginMdm";
    private static final String ACTION_CHECK = "check_active";
    private static final String ACTION_ACTIVE = "active";
    private static final String ACTION_COMMAND = "command";

    private static String MESSAGE_TYPE_KEY = "messageType";
    private static String MESSAGE_TYPE_MDM = "MDM";

    private Context getApplicationContext() {
        return this.cordova.getActivity().getApplicationContext();
    }

    @Override
    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);
        AppLog.i(" MDM插件 启动....");
        // TODO

        if (!MdmCmdExecutor.getInstance().isActive()) {
            // this.cordova.setActivityResultCallback(PluginMdm.this);
            MdmCmdExecutor.getInstance().active((Activity) webView.getContext());
        }
    }

//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        AppLog.i(TAG, "receive result");
//        if (requestCode == MdmCmdExecutor.REQUEST_CODE_MDM) {
//            if (resultCode == Activity.RESULT_OK) {
//                AppLog.i("mdm 激活成功");
//            } else {
//                AppLog.i("mdm 未激活");
//            }
//        } else {
//            super.onActivityResult(requestCode, resultCode, data);
//        }
//    }

    @Override
    public boolean execute(String action, String rawArgs, CallbackContext callbackContext) throws JSONException {
        AppLog.d(TAG, "plugin Mdm, action=" + action + ",rawArgs=" + rawArgs);
        if (ACTION_CHECK.equals(action)) {
            if (MdmCmdExecutor.getInstance().isActive()) {
                callbackContext.success("true");
            } else {
                callbackContext.success("false");
            }
            return true;
        }

        //
        if (ACTION_ACTIVE.equals(action)) {
            AppLog.e(TAG, "action-active ---- pluginMdm");
            if (!MdmCmdExecutor.getInstance().isActive()) {
                MdmCmdExecutor.getInstance().active((Activity) webView.getContext());

                callbackContext.success();
            }
            return true;
        }

        // 执行指令
        if (ACTION_COMMAND.endsWith(action)) {
            ModelMdmCmd cmd = ModelMdmCmd.getMdmCommand(rawArgs);
            if (cmd == null) {
                callbackContext.error("参数不正确");
            } else {
                boolean cmdResult = false;
                try {
                    cmdResult = MdmCmdExecutor.getInstance().exec(cmd);
                } catch (Exception e) {
                    e.printStackTrace();//无须处理
                }
                if (cmdResult) {
                    callbackContext.success();
                } else {
                    callbackContext.error("执行 mdm 失败");
                }
            }
            return true;
        }

        AppLog.e(TAG, "未定义的action = " + action);
        return false;
    }

}
