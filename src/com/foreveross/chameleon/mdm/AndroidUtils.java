package com.foreveross.chameleon.mdm;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.BatteryManager;
import android.os.Environment;
import android.os.StatFs;
import android.provider.Settings;

import java.io.File;

/**
 * project: android_api_learn
 * ate: 14-10-8
 * author: wzq
 * description: TODO
 */
public class AndroidUtils {

    private final static  String TAG = "AndroidUtils";
    private  Context context;
    private static AndroidUtils mInstance;

    public static AndroidUtils getInstance(Context context) {
        if(mInstance == null) {
            mInstance = new AndroidUtils(context);
        }
        return mInstance;
    }

    private AndroidUtils(Context context) {
        this.context = context;
    }

    /**
     * 电池信息
     *
     * @return percent of battery level like 70%
     */
    public String getBatteryInfo() {
        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = context.registerReceiver(null, ifilter);

    /*
        // Are we charging / charged?
        int status = batteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
        boolean isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING ||
                status == BatteryManager.BATTERY_STATUS_FULL;

        // How are we charging?
        int chargePlug = batteryStatus.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
        boolean usbCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_USB;
        boolean acCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_AC;
    */


        int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
        float batteryPct = level / (float) scale;

        return (batteryPct * 100) + " %";
    }


    public  boolean externalMemoryAvailable() {
        return Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED);
    }

    /**
     * 获取内部存储(非外置SD卡)可用容量
     *
     * @return 如 2003M
     */
    public  String getAvailableInternalMemorySize() {
        File path = Environment.getDataDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize = stat.getBlockSize();
        long availableBlocks = stat.getAvailableBlocks();
        return formatSize(availableBlocks * blockSize);
    }

    /**
     * 获取内部存储(非外置SD卡)总容量
     *
     * @return 如 2003M
     */
    public  String getTotalInternalMemorySize() {
        File path = Environment.getDataDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize = stat.getBlockSize();
        long totalBlocks = stat.getBlockCount();
        return formatSize(totalBlocks * blockSize);
    }

    /**
     * 获取内部存储(外置SD卡)可用容量
     *
     * @return 如 2003M
     */
    public  String getAvailableExternalMemorySize() {
        if (externalMemoryAvailable()) {
            File path = Environment.getExternalStorageDirectory();
            StatFs stat = new StatFs(path.getPath());
            long blockSize = stat.getBlockSize();
            long availableBlocks = stat.getAvailableBlocks();
            return formatSize(availableBlocks * blockSize);
        } else {
            return "no external memory";
        }
    }

    /**
     * 获取内部存储(外置SD卡)总容量
     *
     * @return 如 2003MB / 100KB
     */
    public  String getTotalExternalMemorySize() {
        if (externalMemoryAvailable()) {
            File path = Environment.getExternalStorageDirectory();
            StatFs stat = new StatFs(path.getPath());
            long blockSize = stat.getBlockSize();
            long totalBlocks = stat.getBlockCount();
            return formatSize(totalBlocks * blockSize);
        } else {
            return "no external memory";
        }
    }

    private  String formatSize(long size) {
        String suffix = null;

        if (size >= 1024) {
            suffix = "KB";
            size /= 1024;
            if (size >= 1024) {
                suffix = "MB";
                size /= 1024;
            }
        }

        StringBuilder resultBuffer = new StringBuilder(Long.toString(size));

        int commaOffset = resultBuffer.length() - 3;
        while (commaOffset > 0) {
            resultBuffer.insert(commaOffset, ',');
            commaOffset -= 3;
        }

        if (suffix != null) resultBuffer.append(suffix);
        return resultBuffer.toString();
    }


    /** wifi mac地址 */
    public  String getWifiMac() {
        WifiManager manager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        WifiInfo info = manager.getConnectionInfo();
        String address = info.getMacAddress();
        if (address == null) {
            address = "wifi disabled";
        }
        return address;
    }

    public String getBluetoothMac() {
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        String address = bluetoothAdapter.getAddress();
        if (address == null) {
            address = "BlueTooth disable";
        }
        return address;
    }

    public  String getDeviceId() {
        String androidid = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        return androidid;
    }

    public  String isRoot() {
        return Helper_root.isRoot() + "";
    }
}
