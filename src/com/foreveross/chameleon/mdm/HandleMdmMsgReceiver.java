package com.foreveross.chameleon.mdm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.foreveross.chameleon.utils.AppLog;
import org.json.JSONObject;

/**
 * Created by zhouzhineng on 14-9-10.
 */
public class HandleMdmMsgReceiver extends BroadcastReceiver {

    public final static String TAG = "HandleMdmMsgReceiver";

    public final static String ACTION_HANDLE_MDM_MSG = "com.mdmlib.mdm.action_handle_mdm_msg";


    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action.equals(ACTION_HANDLE_MDM_MSG)) {
            String jsonMdm = intent.getStringExtra("jsonMdm");
            AppLog.d(TAG, "the mdm json =====>" + jsonMdm);
            try {

                JSONObject mdmObj = new JSONObject(jsonMdm);
                if (jsonMdm != null) {
                    AppLog.i(TAG, "收到 MDM");
                    new MdmHandlerEntry(mdmObj).handleMdm();
                    return;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }
}
