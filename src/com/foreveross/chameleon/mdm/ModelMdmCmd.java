package com.foreveross.chameleon.mdm;

import com.foreveross.chameleon.utils.AppLog;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author: wzq
 * @date: 14-7-31
 * description: 命令 数据模型
 */
public class ModelMdmCmd {
    public final static String TAG = "ModelMdmCmd";
    MdmCmdType cmdType; // 命令
    String cmdArg; // 命令对应的参数 int or string
    /**
     * 解析出包含命令和参数的命令【命令+参数】
     *
     * @param str json格式的字符串,具体格式参考文档
     *          {
     *            ...
     *            cmdArgsPreserved:
     *            {
     *               "type":"mdm",
     *               "content":
     *               {
     *                 "命令类型":"命令参数"
     *               }
     *            }
     *          }
     */
    public static ModelMdmCmd getMdmCommand(String str) {
        final String TAG_TYPE = "type";
        final String VALUE_MDM = "mdm";
        final String TAG_CONTENT = "content";
        final String TAG_PRESERVED = "cmdArgsPreserved";
        str = str.trim();

        // 去除首位的"",发现从插件传递过来的string，强化含有引号
        if (str.startsWith("\"") && str.endsWith("\"")) {
            AppLog.i(TAG, "命令以\\开头结尾");
            str = str.substring(1, str.length() - 1);
        }

        try {
            JSONObject jsonObject = new JSONObject(str);
            if (jsonObject.has(TAG_PRESERVED)) {
                AppLog.i(TAG, "has cmdArgsPreserved");
                try {
                    jsonObject = (JSONObject) jsonObject.get(TAG_PRESERVED);
                } catch (JSONException e) {
                    AppLog.e(TAG, "cmdArgsPreserved 不能正常转换为JSON对象");
                    e.printStackTrace();
                    return null;
                }
            }

            // 现在jsonObject含有2个child: code 和 content
            String type = jsonObject.getString(TAG_TYPE);
            if (!VALUE_MDM.equals(type)) {
                AppLog.e(TAG, "type != mdm");
                return null;
            }
            JSONObject content = jsonObject.getJSONObject(TAG_CONTENT);
            for (MdmCmdType aType : MdmCmdType.values()) {
                if (content.has(aType.toString())) {
                    ModelMdmCmd mdmCommad = new ModelMdmCmd();
                    mdmCommad.cmdType = aType;
                    Object arg = content.get(aType.toString());
                    if (arg instanceof String) {
                        mdmCommad.cmdArg = (String) arg;
                    }
                    mdmCommad.cmdArg = content.getString(aType.toString());
                    return mdmCommad;
                }
            }
            AppLog.e(TAG, "未发现的命令类型:" + content);
        } catch (JSONException e) {
            e.printStackTrace();
            AppLog.e(TAG, "string 不规范，无法转换成 mdm 命令");
        }
        return null;
    }
}


