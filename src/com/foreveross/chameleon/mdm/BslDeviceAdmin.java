package com.foreveross.chameleon.mdm;
import android.app.admin.DeviceAdminReceiver;
import android.content.Context;
import android.content.Intent;
import com.foreveross.chameleon.utils.AppLog;

/**
 * project:
 * author: wzq
 * date: 2014/7/28
 * description:
 */
public class BslDeviceAdmin extends DeviceAdminReceiver {
    //	implement onEnabled(), onDisabled(),

    public final static String TAG = "BslDeviceAdmin";
    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
    }

    public void onEnabled(Context context, Intent intent) {
        AppLog.d(TAG,"onEnabled");
    }

    public void onDisabled(Context context, Intent intent) {
        AppLog.d(TAG, "onDisabled");
    }
}
