package com.foreveross.chameleon.mdm;

import java.io.File;

/**
 * project: android_api_learn
 * ate: 14-9-18
 * author: wzq
 * description: TODO
 */
// ==============another check method OK===============
// http://stackoverflow.com/questions/19288463/how-to-check-if-android-phone-is-rooted

public class Helper_root {
    /**
     * @return true of false
     */
    public static boolean isRoot() {
        return findBinary("su");
    }

    public static boolean findBinary(String binaryName) {
        boolean found = false;
        if (!found) {
            String[] places = {"/sbin/",
                    "/system/bin/",
                    "/system/xbin/",
                    "/data/local/xbin/",
                    "/data/local/bin/",
                    "/system/sd/xbin/",
                    "/system/bin/failsafe/",
                    "/data/local/"};
            for (String where : places) {
                if (new File(where + binaryName).exists()) {
                    found = true;
                    System.out.println("found bin" + where + binaryName);
                    break;
                }
            }
        }
        return found;
    }
}
//    // helper
//
//    /** @author Kevin Kowalewski */
//    static class Root {
//
//        private static String LOG_TAG = Root.class.getName();
//
//        public static boolean isDeviceRooted() {
//            return checkRootMethod1() || checkRootMethod2() || checkRootMethod3();
//        }
//
//        public static boolean checkRootMethod1() {
//            String buildTags = android.os.Build.TAGS;
//            return buildTags != null && buildTags.contains("test-keys");
//        }
//
//        public static boolean checkRootMethod2() {
//            try {
//                File file = new File("/system/app/Superuser.apk");
//                return file.exists();
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            return false;
//        }
//
//        public static boolean checkRootMethod3() {
//            return new ExecShell().executeCommand(SHELL_CMD.check_su_binary) != null;
//        }
//    }
//
//    public static enum SHELL_CMD {
//        check_su_binary(new String[]{"/system/xbin/which", "su"});
//
//        String[] command;
//
//        SHELL_CMD(String[] command) {
//            this.command = command;
//        }
//    }
//
//    /** @author Kevin Kowalewski */
//    static class ExecShell {
//        private static String LOG_TAG = ExecShell.class.getName();
//
//        public ArrayList<String> executeCommand(SHELL_CMD shellCmd) {
//            String line = null;
//            ArrayList<String> fullResponse = new ArrayList<String>();
//            Process localProcess = null;
//            try {
//                localProcess = Runtime.getRuntime().exec(shellCmd.command);
//            } catch (Exception e) {
//                e.printStackTrace();
//                return null;
//            }
//            BufferedWriter out = new BufferedWriter(new OutputStreamWriter(
//                    localProcess.getOutputStream()));
//            BufferedReader in = new BufferedReader(new InputStreamReader(
//                    localProcess.getInputStream()));
//            try {
//                while ((line = in.readLine()) != null) {
//                    Log.d(LOG_TAG, "--> Line received: " + line);
//                    fullResponse.add(line);
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            Log.d(LOG_TAG, "--> Full response was: " + fullResponse);
//            return fullResponse;
//        }
//    }

