package com.foreveross.chameleon.mdm;

import android.app.Application;
import android.os.Build;

import com.foreveross.chameleon.base.BslApplication;
import com.foreveross.chameleon.utils.AppLog;
import com.foreveross.chameleon.utils.HttpConnection;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import android.content.Context;
import org.apache.http.message.BasicNameValuePair;
import java.util.List;
import java.util.LinkedList;
/**
 * Created by zhouzhineng on 14-10-26.
 */
public class DeviceInfoMgr {
    public final String TAG ="DeviceInfo";
    private Context context;
    private AndroidUtils androidUtils;
    private String receptUrl;       //回执地址
    private static DeviceInfoMgr mInstance;

    public static DeviceInfoMgr getInstance() {
        if(mInstance == null) {
            Application app = BslApplication.getApplication();
            mInstance = new DeviceInfoMgr(app);
        }
        return mInstance;
    }

    private DeviceInfoMgr(Context context) {
        this.context = context;
        androidUtils  = AndroidUtils.getInstance(context);
    }

/**
 * {
 “result”: “true”,
 “desc”:”描述文字abc”,
 “list”:[{
 “ModelName”:“iPad”，
 “SomeKey”:“123”,
 “Model”:“MD531CH”
 }]
 }

 */

    public void setReceptUrl(String receptUrl) {
        this.receptUrl = receptUrl;
    }

    /**
     * 获取设备详细信息
     */

    private String getDeviceInfo() throws JSONException {
        //JSONObject json = new JSONObject();
        JSONObject json = new JSONObject();
        json.put("result",true);
        json.put("desc","设备信息");

        JSONArray items = new JSONArray();
        JSONObject item = new JSONObject();
        // iphone特有 android没有的字段
        item.put("AvailableDeviceCapacity", "");//兼容iphone
        item.put("DeviceCapacity", "");//兼容iphone
        item.put("IsActivationLockEnabled", "");//兼容iphone
        item.put("IsCloudBackupEnabled", "");//兼容iphone
        item.put("IsDeviceLocatorServiceEnabled", "");//兼容iphone
        item.put("IsDoNotDisturbInEffect", "");//兼容iphone
        item.put("UDID", "");//TODO
        item.put("iTunesStoreAccountIsActive", "");


        // 都有的字段
        item.put("BatteryLevel", androidUtils.getBatteryInfo());// 电池
        item.put("BluetoothMAC", androidUtils.getBluetoothMac());// BluetoothMAC
        item.put("BuildVersion", Build.VERSION.RELEASE);
        item.put("DeviceName", Build.USER);
        item.put("EASDeviceIdentifier", androidUtils.getDeviceId());
        item.put("IsSupervised", androidUtils.isRoot());
        item.put("Model", Build.MODEL);
        item.put("ProductName", Build.PRODUCT);
        item.put("SerialNumber", Build.SERIAL);
        item.put("WiFiMAC", androidUtils.getWifiMac());

        // android 特有的字段
        item.put("AvailableInternalMemorySize", androidUtils.getAvailableInternalMemorySize());
        item.put("AvailableExternalMemorySize", androidUtils.getAvailableExternalMemorySize());
        item.put("TotalInternalMemorySize", androidUtils.getTotalInternalMemorySize());
        item.put("TotalExternalMemorySize", androidUtils.getTotalExternalMemorySize());

        items.put(item);

        json.put("list", items);
        // toString
        return json.toString();
    }

    public void execSubmitDeviceInfo() {
        if(receptUrl == null || receptUrl == "") {
            AppLog.e("回执地址不合法!");
            return;
        }
        try{
            String deviceInfo = getDeviceInfo();
            List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
            params.add(new BasicNameValuePair("cmdResult",deviceInfo));
            String retString= HttpConnection.put(receptUrl, params);
            AppLog.d("the responese string == >" + retString);
        }catch(JSONException e) {
            e.printStackTrace();
        }
    }
}
