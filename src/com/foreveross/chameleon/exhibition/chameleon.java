package com.foreveross.chameleon.exhibition;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import com.foreveross.chameleon.base.BslConfig;
import com.foreveross.chameleon.base.EnableCrossDomain;
import com.foreveross.chameleon.mdm.MdmCmdExecutor;
import com.foreveross.chameleon.utils.AppLog;
import com.foreveross.chameleon.utils.AssetTool;
import com.foreveross.chameleon.utils.SpTool;
import org.apache.cordova.Config;
import org.apache.cordova.CordovaActivity;

public class chameleon extends CordovaActivity {
    private final int INTENT_CODE_COPY = 0xAA;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.init();
        EnableCrossDomain.enableContentAsLocal(appView);

        //
        initAtStart();
    }

    /**
     * 程序启动时的初始化:
     * 检测mdm授权->必要时进行mdm授权
     * ->检测sandbox拷贝->必要时进行sandbox拷贝
     */
    private void initAtStart() {
        if (!BslConfig.ENABLE_MDM) {
            initMode();
            return;
        }

        if (MdmCmdExecutor.getInstance().isActive()) {
            AppLog.i("mdm 已授权,检测sandbox模式");
            initMode();
        } else {
            // 因为没有loadurl，所以mdm插件未启动，这里手动启动
            MdmCmdExecutor.getInstance().active(chameleon.this);
            AppLog.i("mdm 未授权,mdm授权后, onActivityResult中进行处理");
        }
    }

    private void initMode() {
        AppLog.i("init mode");
        if (BslConfig.IS_SANDBOX) {
            AppLog.i("sandbox模式");
            if (!SpTool.getSp().getBoolean(BslConfig.FLAG_FILE_COPYED, false)) {
                AppLog.i("文件未拷贝");
                // TODO copy file
                Intent copyIntent = new Intent(this, ActInitMode.class);
                startActivityForResult(copyIntent, INTENT_CODE_COPY);
            } else {
                AppLog.i("文件已拷贝");
                loadStartPage();
            }
        } else {
            AppLog.i("bundle模式");
            loadStartPage();
        }
    }

    private void loadStartPage() {
        String url = Config.getStartUrl();
        url = getUrl(url);
        super.loadUrl(url);
    }

    private String getUrl(String url) {
        AppLog.i("raw url =" + url);
        String cordovaPrefix = "file:///android_asset/www/";
        int index = url.indexOf(cordovaPrefix);
        if (index != -1) {
            url = url.substring(index + cordovaPrefix.length());
        }
        if (!url.startsWith("/")) {
            url = "/" + url;
        }

        // mod
        if (BslConfig.IS_SANDBOX) {
            url = BslConfig.WWW_FOLDER_IN_SD + url;
        } else { //bundle
            url = BslConfig.ASSETS_WWW_FOLDER + url;
        }

        if (BslConfig.IS_CRYPT) {
            url = url.startsWith("/") ? url : "/" + url;
            url = BslConfig.SCHEME_PROVIDER + url;
        } else {
            if (BslConfig.IS_SANDBOX) {
                url = url.startsWith("/") ? url : "/" + url;
                url = BslConfig.FILE_SCHEME + url;
            }
        }
        AppLog.i("chameleon, url=" + url);
        return url;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == MdmCmdExecutor.REQUEST_CODE_MDM) {
            if (resultCode == RESULT_OK) {
                AppLog.i("mdm 激活成功");
            } else {
                AppLog.i("mdm 未激活");
            }

            initMode();
        } else if (requestCode == INTENT_CODE_COPY) {
            if (resultCode == RESULT_OK) {
                AppLog.i("拷贝成功");
            } else {
                AppLog.i("拷贝失败");
            }
            initMode();
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}

