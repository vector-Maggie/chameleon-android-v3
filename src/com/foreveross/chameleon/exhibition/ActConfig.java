package com.foreveross.chameleon.exhibition;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.foreveross.chameleon.utils.PushConstants;

/**
 * Created by wzq on 14-8-19
 * 配置页面
 */
public class ActConfig extends Activity {
    private Context mCtx;
    EditText etInstantUrl;
    EditText etMdmHostPort;
    Button btnDone;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_config);

        mCtx = this;
        etInstantUrl = (EditText)findViewById(R.id.et_instant_url);
        etMdmHostPort = (EditText)findViewById(R.id.et_mdm_host_port);
        btnDone =(Button) findViewById(R.id.btn_done);

        etInstantUrl.setText(PushConstants.INSTANT_URL);
        etMdmHostPort.setText(PushConstants.MDM_BASE_URL);

        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String instantUrl = etInstantUrl.getText().toString();
                PushConstants.INSTANT_URL = instantUrl;

                String mdmUrl = etMdmHostPort.getText().toString();
                PushConstants.MDM_BASE_URL = mdmUrl;

                //
                Intent intent = new Intent(mCtx, chameleon.class);
                mCtx.startActivity(intent);
                finish();
            }
        });
    }
}
