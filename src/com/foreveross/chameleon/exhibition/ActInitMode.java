package com.foreveross.chameleon.exhibition;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.widget.Toast;
import com.foreveross.chameleon.base.BslConfig;
import com.foreveross.chameleon.utils.AppLog;
import com.foreveross.chameleon.utils.AssetTool;
import com.foreveross.chameleon.utils.SpTool;

/**
 * Created by wzq on 14-8-19
 * sandbox模式下,拷贝文件
 */
public class ActInitMode extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_init_mode);

        // 拷贝文件

        new Thread() {
            public void run() {
                AppLog.i("start copying...");
                AssetTool assetTool = new AssetTool();
                assetTool.copyAsset(ActInitMode.this, "www", BslConfig.EXT_STORE);

                // done
                AppLog.i("copy done");
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        SpTool.getSp().write(BslConfig.FLAG_FILE_COPYED, true);
                        // 返回
                        setResult(Activity.RESULT_OK);
                        finish();
                    }
                });
            }
        }.start();
    }

    /**
     * 因为这里需要进行耗时操作，所以禁止返回
     */
    @Override
    public void onBackPressed() {
        //
        Toast.makeText(ActInitMode.this, "正在初始化，请稍等..", Toast.LENGTH_SHORT).show();
    }
}
