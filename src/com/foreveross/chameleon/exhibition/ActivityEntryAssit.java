package com.foreveross.chameleon.exhibition;

import android.content.Context;
import android.content.res.AssetManager;
import com.foreveross.chameleon.base.BslConfig;
import com.foreveross.chameleon.utils.AesTool;
import com.foreveross.chameleon.utils.AppLog;
import com.foreveross.chameleon.utils.AssetTool;
import com.foreveross.chameleon.utils.IOUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

/**
 * Created by wzq on 14-8-19
 * 升级/安装 时执行的文件拷贝
 */
class ActivityEntryAssit {
    // 完全拷贝
    static void copyAll(Context mCtx) {
        AssetTool assetTool = new AssetTool();
        assetTool.copyAsset(mCtx, "www", BslConfig.EXT_STORE);
    }

    // 部分拷贝, plugins文件夹 cordova.js cordova_plugins.js
    // www/xxx/文件夹下,如果含有 CubeModule.json,并且build字段较小，则更新
    static void copyByVersion(Context mCtx) {
        AssetTool assetTool = new AssetTool();
        assetTool.copyAsset(mCtx, "www/plugins", BslConfig.EXT_STORE);
        assetTool.copyAsset(mCtx, "www/cordova.js", BslConfig.EXT_STORE);
        assetTool.copyAsset(mCtx, "www/cordova_plugins.js", BslConfig.EXT_STORE);

        //
        AssetManager assetManager = mCtx.getAssets();
        String wwwFolder[] = null;
        try {
            wwwFolder = assetManager.list("www");
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (wwwFolder == null || wwwFolder.length == 0) {
            return;
        }

        final String CUBE_MODUEL_JSON = "CubeModule.json";
        for (String aFileInWww : wwwFolder) {
            String[] child = null;
            try {
                child = assetManager.list("www/" + aFileInWww);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (child == null || child.length == 0) {
                continue; // not a folder or empty folder
            }
            if (!Arrays.asList(child).contains(CUBE_MODUEL_JSON)) {
                continue; // no CubeModule.json
            }

            AppLog.i("has CubeModule.json : www/" + aFileInWww);
            final String folder = "www/" + aFileInWww;
            final String jsonFile = folder + "/" + CUBE_MODUEL_JSON;
            try {
                // read CUBE_MODUEL_JSON => String =>JSON
                File f = new File(BslConfig.EXT_STORE + "/" + jsonFile);
                InputStream inSd = new FileInputStream(f);
                int savedBuild = getBuildFromCubeModule(inSd);
                inSd.close();

                InputStream in = assetManager.open(jsonFile);
                int currBuild = getBuildFromCubeModule(in);
                in.close();

                AppLog.i("copy file : www/" + aFileInWww);
                AppLog.i("currBuild=" + currBuild + ",savedBuild=" + savedBuild);
                if (currBuild > savedBuild) {
                    assetTool.copyAsset(mCtx, folder, BslConfig.EXT_STORE);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    // 获取CubeModule.json中到build字段的 int 值
    static int getBuildFromCubeModule(InputStream in) throws IOException, JSONException {
        final String BUILD = "build";
        //
        InputStream targetIn;
//        targetIn = AesTool.getInstance().decryptStream(in);
        targetIn = AesTool.decryptStream(in);
        if (targetIn == null) {
            targetIn = in;
        }
        String str = IOUtils.stream2String(targetIn, "UTF-8");
        AppLog.i("getBuildFromCubeModule jsonstr = " + str);
        JSONObject json = new JSONObject(str);
        return json.getInt(BUILD);
    }
}
