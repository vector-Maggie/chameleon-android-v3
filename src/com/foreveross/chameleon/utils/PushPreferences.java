package com.foreveross.chameleon.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

import com.foreveross.chameleon.base.PushApplication;

//import com.foreveross.bsl.util.Preferences;
//import com.foss.AppLog;
//import com.foss.ChameleonApplication;

/**
 * @author 黄黎
 * @version 创建时间：2014年5月19日 下午2:17:09
 *          类说明
 */
public class PushPreferences {

    public final static String TAG = "PushPreferences";

    private SharedPreferences preference;
    private String currentUser;

    public PushPreferences() {
        //this(ChameleonApplication.getApplication());
        this(PushApplication.getApplication());
    }

    public PushPreferences(Context context) {
        preference = PreferenceManager.getDefaultSharedPreferences(context);
        String uString = "";
        if (uString == null || uString.equals("")) {
            uString = "guest";
        }
        currentUser = uString;
    }

    public String getUserName() {
        return currentUser;
    }

    public void saveToken(String token) {
        Editor editor = preference.edit();
        editor.putString(PushConstants.PUSH_TOKEN + currentUser, token);
        editor.commit();
    }

    public String getToken() {
        AppLog.i(TAG, "currUserName=" + currentUser);
        return preference.getString(PushConstants.PUSH_TOKEN + currentUser, "");
    }

    public int getExpiedTime() {
        return preference.getInt(PushConstants.EXPIRED_TIME + currentUser, 0);
    }

    public void saveExpiredTime(int time) {
        Editor editor = preference.edit();
        editor.putInt(PushConstants.EXPIRED_TIME + currentUser, time);
        editor.commit();
    }

    public void saveTokenCreateTime(String time) {
        Editor editor = preference.edit();
        editor.putString(PushConstants.TOKEN_CREATE_TIME + currentUser, time);
        editor.commit();
    }

    public String getTokenCreateTime() {
        return preference.getString(PushConstants.TOKEN_CREATE_TIME + currentUser, "");
    }

    public int getTempPort() {
        return preference.getInt(PushConstants.TEMP_PORT + currentUser, 0);
    }

    public void saveTempPort(int port) {
        Editor editor = preference.edit();
        editor.putInt(PushConstants.TEMP_PORT + currentUser, port);
        editor.commit();
    }

    public void saveTempHost(String host) {
        Editor editor = preference.edit();
        editor.putString(PushConstants.TEMP_HOST + currentUser, host);
        editor.commit();
    }

    public String getTempHost() {
        return preference.getString(PushConstants.TEMP_HOST + currentUser, "");
    }

}
