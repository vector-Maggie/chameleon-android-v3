//package com.foreveross.chameleon.utils;
//
//import android.content.Context;
//
//import com.foreveross.chameleon.nodeclient.DeviceCheckinVo;
//import com.foreveross.chameleon.nodeclient.TagEntryVo;
//import com.foreveross.chameleon.zillasdk.Zilla;
//import com.foreveross.chameleon.zillasdk.ZillaDelegate;
//import com.google.gson.Gson;
//
//
//public class CheckInUtil {
//    public final static String TAG = "CheckInUtil";
//
//    public static void registerPush(final Context application, String token, ZillaDelegate delegate) {
//        final DeviceCheckinVo checkinVo = new DeviceCheckinVo();
//        checkinVo.setDeviceId(DeviceInfoUtil.getDeviceId());
//        checkinVo.setAppId(PushConstants.APPKEY);
//        AppLog.d(TAG, "AppKey == " + PushConstants.APPKEY);
//        checkinVo.setChannelId("instant");
//        checkinVo.setDeviceName(android.os.Build.MODEL);
//        checkinVo.setOsName("android");
//        checkinVo.setOsVersion(android.os.Build.VERSION.RELEASE);
//        checkinVo.setPushToken(token);
//        checkinVo.setTags(new TagEntryVo[]{new TagEntryVo("platform", "Android")});
//        Zilla.getZilla().pushCheckIn(application, delegate, new Gson().toJson(checkinVo));
//
//    }
//
//    public static void pushSecurity(final Context application, final String role) {
//        PushPreferences preferences = new PushPreferences(application);
//        String token = preferences.getToken();
//        final DeviceCheckinVo checkinVo = new DeviceCheckinVo();
//        checkinVo.setDeviceId(DeviceInfoUtil.getDeviceId());
//        checkinVo.setAppId(PushConstants.APPKEY);
//        String alias = Preferences.getUserName();
//        checkinVo.setAlias(alias);
//        checkinVo.setChannelId("instant");
//        checkinVo.setDeviceName(android.os.Build.MODEL);
//        checkinVo.setOsName("android");
//        checkinVo.setOsVersion(android.os.Build.VERSION.RELEASE);
//        checkinVo.setPushToken(token);
//        checkinVo.setTags(new TagEntryVo[]{
//                new TagEntryVo("platform", "Android"),
//                new TagEntryVo("role", role)});
//
//
//        ZillaDelegate delegate = new ZillaDelegate() {
//
//            @Override
//            public void requestSuccess(String result) {
//
//            }
//
//            @Override
//            public void requestStart() {
//
//            }
//
//            @Override
//            public void requestFailed(String errorMessage) {
//
//            }
//        };
//
//        Zilla.getZilla().pushCheckIn(application, delegate, new Gson().toJson(checkinVo));
//    }
//
//}
