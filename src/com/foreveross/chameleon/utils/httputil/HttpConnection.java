package com.foreveross.chameleon.utils.httputil;

import com.foreveross.chameleon.utils.AppLog;
import com.foreveross.chameleon.utils.GeneralUtils;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import java.net.SocketTimeoutException;
import java.util.List;

/**
 * @author 黄黎
 * @version 创建时间：2014年5月8日 下午3:50:32
 *          HTTP连接
 */
public class HttpConnection {
    private static final int TIME_OUT = 10000;

    public static String post(String url, List<BasicNameValuePair> paramss) {
        try {
            if (!GeneralUtils.isNetworkConnected()) {
                AppLog.e("HttpConnection", "No NetWork");
                return null;
            }
            HttpClient client = new DefaultHttpClient();
            HttpParams params = client.getParams();
            HttpConnectionParams.setSoTimeout(params, TIME_OUT);
            HttpConnectionParams.setConnectionTimeout(params, TIME_OUT);
            HttpPost post = new HttpPost(url);

            post.addHeader("Content-Type", "application/x-www-form-urlencoded");
            post.addHeader("charset", HTTP.UTF_8);
            post.setEntity(new UrlEncodedFormEntity(paramss, HTTP.UTF_8));
            HttpResponse response = client.execute(post);
            int code = response.getStatusLine().getStatusCode();
            if (code == 301) {
                String newUrl = response.getHeaders("Location")[0].getValue();
                System.out.println("301,redirect:" + url + "==>" + newUrl);
                return post(newUrl, paramss);
            }
            if (code != 200) {
                AppLog.e("HttpConnection", "HTTP CODE: " + code);
                AppLog.e(EntityUtils.toString(response.getEntity(), HTTP.UTF_8));
                return null;
            }
            String resString = EntityUtils.toString(response.getEntity(), HTTP.UTF_8);
            return resString;
        } catch (SocketTimeoutException e) {
            AppLog.e("HttpConnection", "Socket Time Out");
            e.printStackTrace();
        } catch (ConnectTimeoutException e) {
            e.printStackTrace();
            AppLog.e("HttpConnection", "Http Time Out");
        } catch (Exception e) {
            e.printStackTrace();
            AppLog.e("HttpConnection", "Unkonw Exception: " + e);
        }
        return null;
    }

    public static String put(String url, List<BasicNameValuePair> paramss, String contentType, String accept) {
        try {
            if (!GeneralUtils.isNetworkConnected()) {
                AppLog.e("HttpConnection", "No NetWork");
                return null;
            }
            HttpClient client = new DefaultHttpClient();
            HttpParams params = client.getParams();
            HttpConnectionParams.setSoTimeout(params, TIME_OUT);
            HttpConnectionParams.setConnectionTimeout(params, TIME_OUT);
            HttpPut httpPut = new HttpPut(url);

//            httpPut.addHeader("Content-Type", "application/json");
            //httpPut.addHeader("Content-Type", "application/x-www-form-urlencoded");
            httpPut.addHeader("Content-Type", contentType);
            httpPut.addHeader("accept", accept);
            httpPut.setEntity(new UrlEncodedFormEntity(paramss, HTTP.UTF_8));
            HttpResponse response = client.execute(httpPut);
            int code = response.getStatusLine().getStatusCode();
            if (code != 200) {
                AppLog.e("HttpConnection", "HTTP CODE: " + code);
                AppLog.e(EntityUtils.toString(response.getEntity(), HTTP.UTF_8));
                return null;
            }
            String resString = EntityUtils.toString(response.getEntity(), HTTP.UTF_8);
            return resString;
        } catch (SocketTimeoutException e) {
            AppLog.e("HttpConnection", "Socket Time Out");
            e.printStackTrace();
        } catch (ConnectTimeoutException e) {
            e.printStackTrace();
            AppLog.e("HttpConnection", "Http Time Out");
        } catch (Exception e) {
            e.printStackTrace();
            AppLog.e("HttpConnection", "Unkonw Exception: " + e);
        }
        return null;
    }

    public static String get(String url, String contentType, String accept) {
        try {
            if (!GeneralUtils.isNetworkConnected()) {
                AppLog.e("HttpConnection", "No NetWork");
                return null;
            }
            HttpClient client = new DefaultHttpClient();
            HttpParams params = client.getParams();
            HttpConnectionParams.setSoTimeout(params, TIME_OUT);
            HttpConnectionParams.setConnectionTimeout(params, TIME_OUT);
            HttpGet get = new HttpGet(url);
            //get.addHeader("Content-Type", "application/x-www-form-urlencoded");
            get.addHeader("Content-Type", contentType);
            //get.addHeader("Accept", "html/text");
            get.addHeader("Accept", accept);

            HttpResponse response = client.execute(get);
            int code = response.getStatusLine().getStatusCode();
            if (code != 200) {
                AppLog.e("HttpConnection", "HTTP CODE: " + code);
                AppLog.e(EntityUtils.toString(response.getEntity(), HTTP.UTF_8));
                return null;
            }
            String resString = EntityUtils.toString(response.getEntity(), HTTP.UTF_8);
            return resString;
        } catch (SocketTimeoutException e) {
            AppLog.e("HttpConnection", "Socket Time Out");
            e.printStackTrace();
        } catch (ConnectTimeoutException e) {
            e.printStackTrace();
            AppLog.e("HttpConnection", "Http Time Out");
        } catch (Exception e) {
            e.printStackTrace();
            AppLog.e("HttpConnection", "Unkonw Exception: " + e);
        }
        return null;
    }
}

