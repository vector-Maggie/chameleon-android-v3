package com.foreveross.chameleon.utils.httputil;

public interface DownloadCubeJsonSyncListener {

	public void downloadStart();
	
	public void downloadFinish();
	
	public void downloadFail();
}
