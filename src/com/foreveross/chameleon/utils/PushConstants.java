package com.foreveross.chameleon.utils;

public class PushConstants {
    public final static String TAG = "PushConstants";

    //测试变色龙推送用的服务器地址和端口
    public final static String TEMP_HOST = "121.199.39.92";
    public final static int TEMP_PORT = 8868;

    // instant token服务器
    public static String INSTANT_URL = "http://121.199.39.92:8869/instant/authenticate";
    // mdm 拉取命令/发送回执地址, checkin 地址
    public static String MDM_BASE_URL = "http://bsl.foreveross.com";//"http://172.1.16.183:8080";//

    public final static String APPKEY = "43ea5ef81c2e036a93de068f7dd739be";
    public final static String SECRET = "ab3194ab-c4c9-4b1f-9a99-5a625bae2c60";
    public final static String APP_PACKAGENAME = "com.foreveross.chameleon";
    public final static String APP_VERSION = "1.1.20";
    public static final String EXPIRED_TIME = "expiredTime";
    public static final String PUSH_TOKEN = "pushToken";
    public static final String TOKEN_CREATE_TIME = "tokenCreateTime";
    /**
     * 变色龙长连接服务器的认证地址
     */
    public final static String BASE_WS = "http://115.28.1.119:18860/";
    public final static String AUTH = BASE_WS + "mam/api/mam/clients/apps/android/" + APP_PACKAGENAME + "/" + APP_VERSION + "/validate";
    public final static String PUSH_BASE_URL = BASE_WS + "push/api/";
    public final static String GETPUSHMESSAGE = PUSH_BASE_URL + "push-msgs/none-receipts/";
    public final static String CHECKIN_URL = PUSH_BASE_URL + "checkinservice/checkins";
    public final static String FEEDBACK_URL = PUSH_BASE_URL + "receipts";

    public final static String ACTION_REC_MESSAGE = "com.foreveross.push.recv_msg";
}
