package com.foreveross.chameleon.utils;

/**
 * 简单自定义日志 有开关
 *
 * @author wzq
 */
public class AppLog {
    private static boolean ENABLE_LOGCAT = true;//日志开关
    private static final String TAG = "chameleonV3";
    /**
     * 不要修改此值,日志输出长度控制，logcat对长string会截断
     */
    public static final int LOG_MAX_LEN = 3000;


    public static void v(String tag, String msg) {
        v(tag + "  " + (msg == null ? "" : msg));
    }

    public static void d(String tag, String msg) {
        d(tag + " " + (msg == null ? "" : msg));
    }

    public static void i(String tag, String msg) {
        i(tag + "  " + (msg == null ? "" : msg));
    }

    public static void w(String tag, String msg) {
        w(tag + "  " + (msg == null ? "" : msg));
    }

    public static void e(String tag, String msg) {
        e(tag + "  " + (msg == null ? "" : msg));
    }

    public static void v(String msg) {
        if (ENABLE_LOGCAT) {
            showLog(msg, LOGV);
        }
    }

    public static void d(String msg) {
        if (ENABLE_LOGCAT) {
            showLog(msg, LOGD);
        }
    }

    public static void i(String msg) {
        if (ENABLE_LOGCAT) {
            showLog(msg, LOGI);
        }
    }

    public static void w(String msg) {
        if (ENABLE_LOGCAT) {
            showLog(msg, LOGW);
        }
    }

    public static void e(String msg) {
        if (!ENABLE_LOGCAT) {
            return;
        }

        showLog(msg, LOGE);

        // 打印出错地点
        String errorPoint = "error at " + Thread.currentThread()
                .getStackTrace()[2].getMethodName() + //
                "class:" + Thread.currentThread()
                .getStackTrace()[3].getClassName() + //
                "::" + Thread.currentThread()
                .getStackTrace()[3].getMethodName();

        android.util.Log.e(TAG, errorPoint);
    }

    /**
     * 根据等级，打印日志，保持日志
     *
     * @param msg
     * @param level
     */
    private static void showLog(String msg, int level) {
        // 过长msg的处理
        while (msg.length() >= LOG_MAX_LEN) {
            // 首先打印前LOG_LENGTH个字符
            String str1 = msg.substring(0, LOG_MAX_LEN);
            showLogShort(str1, level);
            msg = msg.substring(LOG_MAX_LEN);
        }

        if (msg.length() != 0) {
            showLogShort(msg, level);
        }
    }

    private static void showLogShort(String str, int level) {
        switch (level) {
            case LOGV:
                android.util.Log.v(TAG, str);
                break;
            case LOGD:
                android.util.Log.d(TAG, str);
                break;
            case LOGI:
                android.util.Log.i(TAG, str);
                break;
            case LOGW:
                android.util.Log.w(TAG, str);
                break;
            case LOGE:
                android.util.Log.e(TAG, str);
                break;
        }

    }

    /**
     * 在方法内部调用，打印调用信息
     */
    public static void whoInvokeMe() {
        if (!ENABLE_LOGCAT) {
            return;
        }

        String callerInfo = Thread.currentThread()
                .getStackTrace()[3].getMethodName() + //
                " called by " + Thread.currentThread()
                .getStackTrace()[4].getClassName() + //
                "::" + Thread.currentThread()
                .getStackTrace()[4].getMethodName();

        android.util.Log.i(TAG, callerInfo);
    }

    /**
     * 打印流程 静态方法中调用会不准
     *
     * @param msg string-msg
     */
    public static void printProcess(String msg) {
        if (!ENABLE_LOGCAT) {
            return;
        }

        String info = Thread.currentThread()
                .getStackTrace()[3].getClassName() + //
                "::" + Thread.currentThread()
                .getStackTrace()[3].getMethodName();
        if (msg != null) {
            info += "  " + msg;
        }
        d(info);
    }

    /**
     * 打印流程 静态方法中调用会不准
     *
     * @param level 0~4 代表v/d/i/w/e
     * @param msg   string-msg
     */
    public static void printProcess(int level, String msg) {
        if (!ENABLE_LOGCAT) {
            return;
        }

        String info = Thread.currentThread()
                .getStackTrace()[3].getClassName() + //
                "::" + Thread.currentThread()
                .getStackTrace()[3].getMethodName();
        if (msg != null) {
            info += "  " + msg;
        }

        switch (level) {
            case LOGE:
                e(info);
                break;
            case LOGW:
                w(info);
                break;
            case LOGD:
                d(info);
                break;
            case LOGV:
                v(info);
                break;
            case LOGI:
            default:
                i(info);
                break;
        }
    }

    /**
     * 保存的日志等级
     */
    public static final int LOGV = 0;
    public static final int LOGD = 1;
    public static final int LOGI = 2;
    public static final int LOGW = 3;
    public static final int LOGE = 4;
}
