package com.foreveross.chameleon.utils;


import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by wzq on 14-8-19
 * 读取 res/raw/cube.properties文件中到 key-value pair
 */
public class PropReader {
    private final static String TAG = PropReader.class.getSimpleName();
    private final static String path = "res/raw/cube.properties";

    private static PropReader instance = new PropReader();

    public static PropReader getInstance() {
        return instance;
    }

    private Properties properties;

    private PropReader() {
        //TODO: read key from config file
        InputStream in = getClass().getClassLoader().getResourceAsStream(path);
        properties = new Properties();
        try {
            properties.load(in);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getProp(String key) {
        String value = properties.getProperty(key);
        AppLog.i(TAG, "key=" + key + ",value=" + value);
        return value;
    }
}
