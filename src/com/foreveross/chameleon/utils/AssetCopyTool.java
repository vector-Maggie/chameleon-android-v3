package com.foreveross.chameleon.utils;

import android.content.Context;
import android.content.res.AssetManager;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * asset相关工具方法,用于拷贝assets目录下的 文件/文件夹 到 SD卡
 *
 * @date: 2014年5月7日
 * @author: wzq
 */
public class AssetCopyTool {
    private static final String tag = AssetCopyTool.class.getSimpleName();

    private static Context mCtx;
    private String mRootDir;

    /**
     * 将assets目录整个拷贝到另外一个目录
     *
     * @param ctx
     * @param rootDir
     */
    public void copyAsset(Context ctx, String rootDir) {
        mCtx = ctx;
        mRootDir = rootDir;
        copyFileOrDir("");
    }

    /**
     * 将assets下的某个目录 拷贝到sd卡的另外一个目录
     *
     * @param ctx
     * @param rootDir
     */
    public void copyAsset(Context ctx, String filePath, String rootDir) {
        mCtx = ctx;
        mRootDir = rootDir;
        copyFileOrDir(filePath);
    }

    private void copyFileOrDir(String path) {
        AssetManager assetManager = mCtx.getAssets();
        String assets[] = null;
        try {
            assets = assetManager.list(path);
            // TODO 如果path="",则列举根目录。根目录下有 webkit sounds images是系统目录
            // 这些目录我们不需要
            if ("".equals(path)) {
                ArrayList<String> list = new ArrayList<String>();
                List<String> filterList = new ArrayList<String>();
                filterList.add("webkit");
                filterList.add("sounds");
                filterList.add("images");
                for (String s : assets) {
                    if (!filterList.contains(s)) {
                        list.add(s);
                    }
                }
                assets = new String[list.size()];
                list.toArray(assets);
            }
            if (assets.length == 0) {
                copyFile(path);
            } else {
                // dir
                File dir = new File(mRootDir, path);
                if (!dir.exists()) {
                    dir.mkdir();
                }
                for (int i = 0; i < assets.length; ++i) {
                    // asset的根目录列举是用copyFileOrDir(""),根目录下的目录不用"/"
                    String folderPath;
                    if ("".equals(path)) {
                        folderPath = assets[i];
                    } else {
                        folderPath = path + "/" + assets[i];
                    }
                    copyFileOrDir(folderPath);
                }
            }
        } catch (IOException ex) {
            AppLog.e(tag, ex.toString());
            ex.printStackTrace();
        }
    }

    private void copyFile(String filename) {
        AssetManager assetManager = mCtx.getAssets();
        InputStream in = null;
        OutputStream out = null;
        try {
            in = assetManager.open(filename);
            String newFileName = mRootDir + "/" + filename;
            // TODO:删除之前的
            File file = new File(newFileName);
            if (file.exists()) {
                file.delete();
            }
            if (!file.getParentFile().exists()) {
                file.getParentFile().mkdirs();
            }
            out = new FileOutputStream(newFileName);
            byte[] buffer = new byte[1024];
            int read;
            while ((read = in.read(buffer)) != -1) {
                out.write(buffer, 0, read);
            }
            in.close();
            out.flush();
            out.close();
        } catch (Exception ex) {
            AppLog.e(tag, ex.toString());
            ex.printStackTrace();
        }
    }
}
