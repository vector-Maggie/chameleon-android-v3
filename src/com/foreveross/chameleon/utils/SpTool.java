package com.foreveross.chameleon.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import com.foreveross.chameleon.base.BslApplication;

/**
 * Created by wzq on 14-8-20.
 * SharedPreference 代理类,方便使用
 */
public class SpTool {
    SharedPreferences sp;

    private SpTool(SharedPreferences sp) {
        this.sp = sp;
    }

    private static SpTool tool;

    /**
     * 使用默认sp文件
     */
    public static SpTool getSp() {
        if (tool == null) {
            SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(BslApplication.getInstance());
            tool = new SpTool(sp);
        }
        return tool;
    }

    /** 使用指定sp文件 */
    public static SpTool getSp(String file) {
        SharedPreferences sp = BslApplication.getInstance().getSharedPreferences(file, Context.MODE_PRIVATE);
        return new SpTool(sp);
    }

    public String getString(String key) {
        return sp.getString(key, null);
    }

    public int getInt(String key, int defValue) {
        return sp.getInt(key, defValue);
    }

    public boolean getBoolean(String key, boolean defValue) {
        return sp.getBoolean(key, defValue);
    }

    public void write(String key, String value) {
        sp.edit().putString(key, value).commit();
    }

    public void write(String key, boolean value) {
        sp.edit().putBoolean(key, value).commit();
    }

    public void write(String key, int value) {
        sp.edit().putInt(key, value).commit();
    }
}
