package com.foreveross.chameleon.utils;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Build;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;

import java.util.UUID;

import com.foreveross.chameleon.base.PushApplication;

public class DeviceInfoUtil {

    //{@link #getDeviceId 是一个幂等方法，所以缓存以下 }
    private static String uniqueId = null;
    private static boolean isPad = false;


    public static String getDeviceId() {
        return uniqueId;

    }

    public static boolean isPad() {
        return isPad;
    }

    static {
        init();
    }

    private static void init() {
        // deviceId
        generateDeviceId();

        // isPad
        checkIsPad();
        checkIsPadV2();
    }

    /**
     * 生产 deviceId
     */
    private static void generateDeviceId() {
        //Context context = ChameleonApplication.getApplication();
    	Context context = PushApplication.getApplication();
        String androidid = Secure.getString(context.getContentResolver(), Secure.ANDROID_ID);
        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String tmDevice, tmSerial;
        tmDevice = "" + tm.getDeviceId();
        tmSerial = "" + tm.getSimSerialNumber();
        AppLog.d("cube", " androidid=" + androidid + " tmDevice=" + tmDevice + " tmSerial=" + tmSerial);
        if(androidid == null) {
            androidid = "";
        }
        if(tmSerial == null) {
            tmSerial = "";
        }
        AppLog.d("cube", " androidid=" + androidid + " tmDevice=" + tmDevice + " tmSerial=" + tmSerial);
        UUID deviceUuid = new UUID(androidid.hashCode(), ((long) tmDevice.hashCode() << 32) | tmSerial.hashCode());
        uniqueId = deviceUuid.toString();
        AppLog.d("cube", "uniqueId = " + uniqueId);
    }

    // 检测device 是否为pad
    private static void checkIsPad() {
        //Context context = ChameleonApplication.getApplication();
    	Context context = PushApplication.getApplication();
        // {treat Galaxy Note as phone.N7000
        String model = Build.MODEL;
        if(model.contains("N7000") || model.contains("I9200") || model.contains("C6802") || model.contains("C6833")) {
            isPad = false;
            return;
        }
        Configuration config = context.getResources().getConfiguration();
        if((config.screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_NORMAL) {
            isPad = false;
        } else if((config.screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_SMALL) {
            isPad = false;
        } else if((config.screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == 4) {
            isPad = true;
        } else {
            isPad = true;
        }
    }

    // 检测device 是否为pad
    private static void checkIsPadV2() {
        final double MIN_PAD_SIZE = 6.5;
        //Context context = ChameleonApplication.getApplication();
        Context context = PushApplication.getApplication();
        DisplayMetrics dm = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(dm);
        int screenWidth = dm.widthPixels;
        int screenHeight = dm.heightPixels;
        float density = dm.density; // 屏幕密度（0.75 / 1.0 / 1.5）
        double diagonalPixels = Math.sqrt(Math.pow(screenWidth, 2) + Math.pow(screenHeight, 2));
        double screenSize = diagonalPixels / (160 * density);
        isPad = screenSize > MIN_PAD_SIZE;
    }

    public static void setSceenSize(Activity activity) {
        WindowManager m = activity.getWindowManager();
        Display d = m.getDefaultDisplay(); // 为获取屏幕宽、高
        LayoutParams p = activity.getWindow().getAttributes(); // 获取对话框当前的参数值
        if(isPad()) {
            p.height = (int) (d.getHeight() * 0.8); // 高度设置为屏幕的1.0
            p.width = (int) (d.getWidth() * 0.5); // 宽度设置为屏幕的0.8
        }
        p.alpha = 1.0f; // 设置本身透明度
        p.dimAmount = 0.7f; // 设置黑暗度
        activity.getWindow().setAttributes(p);
    }
}
