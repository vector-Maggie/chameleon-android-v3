package com.foreveross.chameleon.utils;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import java.net.SocketTimeoutException;
import java.util.List;


/**
 * @author 黄黎
 * @version 创建时间：2014年5月8日 下午3:50:32
 *          HTTP连接
 */
@Deprecated
public class HttpConnection {

    public static String post(String url, List<BasicNameValuePair> paramss) {
        try {
            if (!AndroidTool.isNetworkConnected()) {
                AppLog.e("No NetWork");
                return null;
            }
            HttpClient client = new DefaultHttpClient();
            HttpParams params = client.getParams();
            HttpConnectionParams.setSoTimeout(params, 3000);
            HttpConnectionParams.setConnectionTimeout(params, 3000);
            HttpPost post = new HttpPost(url);

            // post.addHeader("Content-Type", "application/json");
            post.addHeader("charset", HTTP.UTF_8);
            post.setEntity(new UrlEncodedFormEntity(paramss, HTTP.UTF_8));
            HttpResponse response = client.execute(post);
            int code = response.getStatusLine().getStatusCode();
            if (code != 200) {
                AppLog.e("HTTP CODE: " + code);
                return null;
            }
            String resString = EntityUtils.toString(response.getEntity(), HTTP.UTF_8);
            return resString;
        } catch (SocketTimeoutException e) {
            AppLog.e("Socket Time Out");
            e.printStackTrace();
        } catch (ConnectTimeoutException e) {
            e.printStackTrace();
            AppLog.e("Http Time Out");
        } catch (Exception e) {
            e.printStackTrace();
            AppLog.e("Unkonw Exception: " + e);
        }
        return null;
    }

    public static String put(String url, List<BasicNameValuePair> paramss) {
        try {
            if (!AndroidTool.isNetworkConnected()) {
                AppLog.e("No NetWork");
                return null;
            }
            HttpClient client = new DefaultHttpClient();
            HttpParams params = client.getParams();
            HttpConnectionParams.setSoTimeout(params, 3000);
            HttpConnectionParams.setConnectionTimeout(params, 3000);
            HttpPut httpPut = new HttpPut(url);

            // post.addHeader("Content-Type", "application/json");
            httpPut.addHeader("charset", HTTP.UTF_8);
            httpPut.setEntity(new UrlEncodedFormEntity(paramss, HTTP.UTF_8));
            HttpResponse response = client.execute(httpPut);
            int code = response.getStatusLine().getStatusCode();
            if (code != 200) {
                AppLog.e("HTTP CODE: " + code);
                return null;
            }
            String resString = EntityUtils.toString(response.getEntity(), HTTP.UTF_8);
            return resString;
        } catch (SocketTimeoutException e) {
            AppLog.e("Socket Time Out");
            e.printStackTrace();
        } catch (ConnectTimeoutException e) {
            e.printStackTrace();
            AppLog.e("Http Time Out");
        } catch (Exception e) {
            e.printStackTrace();
            AppLog.e("Unkonw Exception: " + e);
        }
        return null;
    }

    public static String get(String url) {
        try {
            if (!AndroidTool.isNetworkConnected()) {
                AppLog.e("No NetWork");
                return null;
            }
            HttpClient client = new DefaultHttpClient();
            HttpParams params = client.getParams();
            HttpConnectionParams.setSoTimeout(params, 3000);
            HttpConnectionParams.setConnectionTimeout(params, 3000);
            HttpGet get = new HttpGet(url);

            HttpResponse response = client.execute(get);
            int code = response.getStatusLine().getStatusCode();
            if (code != 200) {
                AppLog.e("HTTP CODE: " + code);
                return null;
            }
            String resString = EntityUtils.toString(response.getEntity(), HTTP.UTF_8);
            return resString;
        } catch (SocketTimeoutException e) {
            AppLog.e("Socket Time Out");
            e.printStackTrace();
        } catch (ConnectTimeoutException e) {
            e.printStackTrace();
            AppLog.e("Http Time Out");
        } catch (Exception e) {
            e.printStackTrace();
            AppLog.e("Unkonw Exception: " + e);
        }
        return null;
    }
}

