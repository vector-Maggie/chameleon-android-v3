package com.foreveross.chameleon.nodeclient;

/**
 * Created by zhouzhineng on 14-8-25.
 * 消息类
 */
public class MessageInfo {
    public final static String TAG = "MessageInfo";

    private String id;
    private String title;
    private String content;
    private String messageType;
    private String extras;

    public final static String ID = "id";
    public final static String TITLE = "title";
    public final static String CONTENT = "content";
    public final static String MSG_TYPE = "messageType";
    public final static String EXTRAS = "extras";
    public final static String DIRC_EXTR_PRO = "directExtrasPropertys";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getExtras() {
        return extras;
    }

    public void setExtras(String extras) {
        this.extras = extras;
    }

    public String getDerectExtrasPropertys() {
        return derectExtrasPropertys;
    }

    public void setDerectExtrasPropertys(String derectExtrasPropertys) {
        this.derectExtrasPropertys = derectExtrasPropertys;
    }

    private String derectExtrasPropertys;



}
