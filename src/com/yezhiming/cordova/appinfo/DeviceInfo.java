package com.yezhiming.cordova.appinfo;

import android.content.Context;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import com.foreveross.chameleon.utils.AppLog;
import com.foreveross.chameleon.utils.PushConstants;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.UUID;

/**
 * This class returns the version number
 */
public class DeviceInfo extends CordovaPlugin {

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals("getDeviceId")) {
            String deviceId = getDeviceId();
            callbackContext.success(deviceId);
            return true;
        }

        if (action.equals("getDeviceDetail")) {
            String deviceId = getDeviceDetail();
            callbackContext.success(deviceId);
            return true;
        }

        if (action.equals("getIp")) {
            String ip = getIp();
            callbackContext.success(ip);
            return true;
        }

        if ("setIp".equals(action)) {
            String ip = args.getString(0);
            AppLog.i("设置ip:=" + ip);
            if (ip.startsWith("http://") || ip.startsWith("https://")) {
                setIp(ip);
                callbackContext.success();
            } else {
                callbackContext.error("ip必须http:// 或 https:// 开头");
            }
            return true;
        }
        AppLog.e("AppInfo", "unknown action=" + action);
        return false;
    }

    private String getIp() {
        return PushConstants.MDM_BASE_URL;
    }

    private void setIp(String host) {
        PushConstants.MDM_BASE_URL = host;
    }

    private String getDeviceId() {
        Context context = webView.getContext();
        String androidid = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String tmDevice, tmSerial;
        tmDevice = "" + tm.getDeviceId();
        tmSerial = "" + tm.getSimSerialNumber();
        AppLog.d("cube", " androidid=" + androidid + " tmDevice=" + tmDevice + " tmSerial=" + tmSerial);
        if (androidid == null) {
            androidid = "";
        }
        AppLog.d("cube", " androidid=" + androidid + " tmDevice=" + tmDevice + " tmSerial=" + tmSerial);
        UUID deviceUuid = new UUID(androidid.hashCode(), ((long) tmDevice.hashCode() << 32) | tmSerial.hashCode());
        return deviceUuid.toString();
    }

    public String getDeviceDetail() {
        final String appId = "43ea5ef81c2e036a93de068f7dd739be"; //TODO
        final String channelId = "instant";
        final String deviceName = android.os.Build.MODEL;
        final String osName = "android";
        final String osVersion = Build.VERSION.RELEASE;
        //final String osVersion = Build.VERSION.SDK_INT;
        JSONObject json = new JSONObject();
        try {
            json.put("appId", appId);
            json.put("channelId", channelId);
            json.put("deviceName", deviceName);
            json.put("osName", osName);
            json.put("osVersion", osVersion);
            json.put("pushToken", "");//for ios, android empty
            return json.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            throw new RuntimeException("JsonException");
        }
    }
}