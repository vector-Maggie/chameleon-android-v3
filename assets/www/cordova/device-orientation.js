define(['butterfly/view'], function(View){

  return View.extend({
    events: {
      "click a[data-action='back']": "goBack",
      "click .start": "startCompass",
      "click .stop": "stopCompass" 
    },
    
    watchID:null,
    initialize:function(){
    	idStr="compassWatchID";
    },
	  onShow: function(){
      console.log("go");
      console.log(this.watchID);
      var watchID=window.sessionStorage[idStr];
      if(watchID!=null){
    	  console.log(watchID);
    	  this.watchID=watchID;
      }
    },

     onSuccess: function(heading) {
      
      var hd = Math.round(heading.magneticHeading);
      var element = document.getElementById('compass');
        element.innerHTML = "Heading: " + hd + "degress";
      
    },

    onError: function(compassError) {
      
      console.log(compassError);
       navigator.compass.clearWatch(this.watchID);
       var element = document.getElementById('compass');
      element.innerHTML="";
      if(compassError.code==CompassError.COMPASS_NOT_SUPPORTED)
      {
        alert("不支持罗盘功能");
      }else if(compassError.code==CompassError.COMPASS_INTERNAL_ERR)
      {
        alert("罗盘内部错误");
      }else
      {
        alert("未知的朝向错误");
      } 
    },

    startCompass: function(){
    	if(this.watchID==null){
         var options = {frequency: 1000}; // Update every 1 second
         this.watchID = navigator.compass.watchHeading(this.onSuccess, this.onError, options);
         window.sessionStorage[idStr]= this.watchID;
    	}
    },

    stopCompass:function(){
      if (this.watchID!== null)
      {
        navigator.compass.clearWatch(this.watchID);
        this.watchID = null;
        // this.onSuccess({ magneticHeading : "Off"});
      }
      window.sessionStorage.removeItem(idStr);
    },

    goBack: function(){
      window.history.back();
    }
  });
});
