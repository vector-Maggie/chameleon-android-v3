define(['butterfly/view'], 
	function(View) {

	return View.extend({
		initialize:function(){
	    	console.log("camera.initialize...");
	    },
	    onShow:function(){
	    	console.log("camera.onShow...");

	    },
	    render:function(){
	    	console.log("camera.render...");
	    },
		events : {
			"click a[data-action='back']" : "goBack",
			"click #fromAlbum" : "getFromAlbum",
			"click #fromCamera" : "getFromCamera"
		},

		getFromAlbum : function() {
			console.log("getFromAlbum...........");
			navigator.camera.getPicture(this.success, this.fail, {
				quality : 50,
				destinationType: Camera.DestinationType.FILE_URI,
				sourceType : Camera.PictureSourceType.PHOTOLIBRARY,
				correctOrientation : true
			});
		},

		getFromCamera : function() {
			console.log("getFromCamera..................");
			navigator.camera.getPicture(this.success, this.fail, {
				quality : 50,
				destinationType: Camera.DestinationType.FILE_URI,
				sourceType : Camera.PictureSourceType.CAMERA,
				correctOrientation : true
			});
		},

		success : function(imageURI) {
			console.log(imageURI);
			this.$("#showPic").show().attr({
				"src" : imageURI
			});
		},
		fail : function(message) {
			alert('Failed because: ' + message);
		},

		goBack : function() {
			window.history.back();
		}
	});
});
