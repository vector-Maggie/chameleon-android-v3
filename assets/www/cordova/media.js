define(['butterfly/view'], function(View){
  return View.extend({
    events: {
    	"click a[data-action='back']": "goBack",
    	"click .startRecord": "recordAudio",
		"click .stopRecord": "stopRecordAudio",
		"click .play": "playAudio",
		"click .pause": "pauseAudio",
		"click .stop": "stopAudio",
		"click .release": "releaseAudio",
		"click .seek": "setSeekTo",
		"click .duration": "getDuration",
		"click .mute": "setVolume0",
		"click .sound": "setVolume1"
    },
    my_media: null,
    isStart: null,
    mediaTimer: null,
    playTime: null,
    isRun: false,
    
	recordAudio: function() {
		var platforms = window.device.platform;
		if (platforms.toLowerCase().indexOf("ios") > -1) {
			if (!this.isStart) {
				var me = this;
				var src = "myrecording.wav";
				if (this.my_media) {
					this.my_media.release();
					this.my_media = null;
				}
						//alert("1")
				var createMedia = function(onMediaCreated, mediaStatusCallback) {
					alert("createMedia");
					if (me.my_media != null) {
						onMediaCreated();
						return;
					}
					if (typeof mediaStatusCallback == 'undefined')
						mediaStatusCallback = null;
					window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fileSystem) {
						fileSystem.root.getFile(src, {create: true,exclusive: false}, 
							function(fileEntry) {
								me.my_media = new Media(fileEntry.fullPath, function() {	
							}, me.onError, mediaStatusCallback);
							onMediaCreated();
						}, me.onError); 
					}, me.onError);
				};
				createMedia(function() {
					me.my_media.startRecord();
				});
				me.isStart = true;
			}
		} 
		else if (platforms.toLowerCase().indexOf("android") > -1) {
			if (!this.isStart) {
				if (this.my_media) {
					this.my_media.release();
					this.my_media = null;
				}
				if(!this.isRun){
					var src = "myrecording.mp3";
				this.my_media = new Media(src, this.onSuccess, this.onError);
				alert("createMedia");
				this.my_media.startRecord();
				// $(".stopRecord").attr("disabled","false");
				this.isStart = true;
				this.isRun=false;
				}
			}
		}
	},

    stopRecordAudio:function (){
    	this.isStart = false;
    	this.isRun=false;
    	if(this.my_media!=null){
    		this.my_media.stopRecord();
    	}
    },

    playAudio: function(){
		var platforms = window.device.platform;
			if (platforms.toLowerCase().indexOf("ios") > -1) {
				this.isStart = false;
				var me = this;
				var src = "myrecording.wav";
				if (this.my_media) {
					this.my_media.stopRecord();
				};
				this.my_media.play();
				if (me.mediaTimer == null) {
					me.mediaTimer=setInterval(function(){
					me.my_media.getCurrentPosition(
						// success callback
						function(position) {
							console.log("position" + position);
							if (position > -1) {
								me.setAudioPosition((position) + " sec");
							}
						},
						// error callback
						function(e) {
							console.log("Error getting pos=" + e);
							me.setAudioPosition("Error: " + e);
						}
					);
					},1000);
					// $(".play").addClass("disabled");
				}

			} else if (platforms.toLowerCase().indexOf("android") > -1) {
				if(!this.isRun){
				this.isRun=true;
				this.isStart = false;
				var me = this;
				var src = "myrecording.mp3";
				if (this.my_media) {
					this.my_media.stopRecord();
					this.my_media = null;
				}
				this.my_media = new Media(src, this.onSuccess, this.onError);
				this.my_media.play();
				
				if(this.playTime!=null){
					me.setAudioPosition(me.playTime+" sec");
					var pauseTime=this.playTime*1000;
					this.my_media.seekTo(pauseTime);
				}
				if(me.mediaTimer==null){
					// me.setAudioPosition("0 sec");
					me.mediaTimer=setInterval(function(){
					me.my_media.getCurrentPosition(
					// success callback
					function(position) {
						console.log("position" + position);
						if (position > -1) {
								if(position==-0.001){	
								me.setAudioPosition("0 sec");
								me.mediaTimer=clearInterval(me.mediaTimer);
								me.mediaTimer = null;
								me.isRun=false;
								me.playTime=0;
							}
							else{
								me.setAudioPosition((position) + " sec");
								me.playTime=position;
							}
						}

					},
					// error callback
					function(e) {
						console.log("Error getting pos=" + e);
						me.setAudioPosition("Error: " + e);
					}
				);
			},1000);
			}
		   
		 }	

		}	
	},

    pauseAudio: function(){
    	this.isStart=false;
    	this.isRun=false;
    	if(this.my_media){
    		this.my_media.pause();
    		clearInterval(this.mediaTimer);
    		this.mediaTimer=null;
    	}
    	// if(this.playTime==null){
    	// 	this.setAudioPosition("0 sec");
    	// }else{
    	// 	this.setAudioPosition(this.playTime+" sec");
    	// }
    	
    },

    stopAudio: function() {
    	this.isRun=false;
    	this.isStart = false;
		if (this.my_media) {
			this.my_media.stop();
		}
		clearInterval(this.mediaTimer);
		this.mediaTimer = null;
		this.playTime = 0;
		this.setAudioPosition("0 sec");
		// $(".play").removeClass("disabled").attr("disabled","false");
	},

	releaseAudio: function() {
		this.isStart=false;
		this.isRun=false;
		if (this.my_media) {
			this.my_media.release();
			this.my_media = null;
			
		}
			clearInterval(this.mediaTimer);
			this.mediaTimer = null;
			this.playTime=0;
			this.setAudioPosition("0 sec");
			// $(".play").removeClass("disabled").attr("disabled","false");
	},

	setVolume0: function() {
		if (this.my_media != null) {
			this.my_media.setVolume(0.0);
		}
	},

	setVolume1: function() {
		if (this.my_media != null) {
			this.my_media.setVolume(1.0);
		}
	},

	setSeekTo: function() {
		if (this.my_media != null) {
		this.my_media.seekTo(10000);
		}
	},

	setAudioPosition: function(position) {
		document.getElementById('audio_position').innerHTML = position;
	},

	getDuration: function() {
		if (this.my_media != null) {
			var dur = this.my_media.getDuration();
			if (dur > 0) {
				document.getElementById('audio_duration').innerHTML = (dur) + " sec";
			}
		}
	},

    onSuccess: function(){
    	console.log("recordAudio():Audio Success");
    },

    onError: function(error){
    	console.log("recordAudio():Audio Error: "+ error.code);

    },
 
    goBack: function(){
    	window.history.back();
    }
  });
});
