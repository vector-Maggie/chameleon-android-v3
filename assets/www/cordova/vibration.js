define(['butterfly/view'], function(View){

  return View.extend({
    events: {
      "click a[data-action='back']": "goBack",
      "click #vibrate": "vibrate"
    },

    onshow: function(){
    	var me=this;
    	console.log("let us go!"); 
    },
    vibrate: function(){
    	navigator.notification.vibrate(1500);
    	console.log("yes,we success!");
    },
    goBack: function(){
      window.history.back();
    }
  });
});
