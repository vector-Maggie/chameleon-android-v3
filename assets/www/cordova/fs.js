define(['butterfly/view'], function(View){
  return View.extend({
    events: {
      "click a[data-action='back']": "goBack",
      "click #write":"writeToFile",
      "click #read":"readFile",
      "click #delete":"deleteFile",
      "click #modified":"getMetadataInfo",
      "click #clear":"clearLog",
      "click #dirReader":"getDirectoryListing"
    },
    
    initialize:function(){
    	fs=this;
      console.log("FileSystem.initialize...");
    },
    render:function(){
    	console.log("FileSystem.render...");
    },
    
    onShow: function () {
        console.log("FileSystem.onShow.........");
        document.addEventListener("deviceready", this.onDeviceReady, false);
    },
    
    onDeviceReady:function() {  
    	console.log("FileSystem.onDeviceReady...........................");
    },
    
    //writeFile
    
    writeToFile:function() {  
      window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, fs.gotFSForWrite, fs.fail);
    },
    gotFileWriter:function(writer) {  
       var userText = $('#userInput').val();  
       console.log(userText);
       writer.seek(writer.length);  
       writer.write('\n\n' + userText);  
       writer.onwriteend = function(evt){  
         fs.$("#result").html("You wrote ' " + userText + " ' at the end of the file.");  
       }   
       fs.$('#userInput').val("");
      },
     gotWriteFileEntry:function(fileEntry) {  
        fileEntry.createWriter(fs.gotFileWriter,fs.fail);
      },
      gotFSForWrite:function(fileSystem) {  
        fileSystem.root.getFile("CordovaSample.txt", {create: true, exclusive: false}, fs.gotWriteFileEntry, fs.fail);  
      },

    //readFile
    
     readFile:function() {
    	 console.log("FileSystem.readFile....");
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, fs.gotFSForRead, fs.fail);  
      },
      
     gotFSForRead:function(fileSystem){
    	console.log("FileSystem.gotFSForRead....");
        fileSystem.root.getFile("CordovaSample.txt", null, fs.gotReadFileEntry, fs.fail);
     },
     gotFileRead:function(file){  
      fs.readDataUrl(file);  
      fs.readAsText(file);  
      console.log("FileSystem.gotFileRead....");
     },
     gotReadFileEntry:function(fileEntry) {  
        console.log("FileSystem.gotReadFileEntry....");
        fileEntry.file(fs.gotFileRead,fs.fail);  
      },
      readDataUrl:function(file) {  
        console.log("FileSystem.readDataUrl....");
        var reader = new FileReader();  
        reader.onloadend = function(evt) {  
        	$("#result").append('<strong>Read as data URL:</strong> <br><pre>' + evt.target.result + '</pre>');
        };
        reader.readAsDataURL(file);  
      },
      readAsText:function(file) {   
        console.log("FileSystem.readAsText....");
        var reader = new FileReader();  
        reader.onloadend = function(evt) {  
          $("#result").append('<strong>Read as data text:</strong> <br><pre>' + evt.target.result + '</pre>');  
        };  
        reader.readAsText(file);  
      },

     //deleteFile
     
     deleteFile:function() {  
         window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, fs.gotFSForRemove, fs.fail);  
     },
     gotFSForRemove:function(fileSystem) {  
       fileSystem.root.getFile("CordovaSample.txt", {create: false, exclusive: false}, fs.gotRemoveFileEntry, fs.removeFail);  
     }, 
      gotRemoveFileEntry:function(fileEntry){  
         fileEntry.remove(  
           function(entry) {  
          fs.$("#result").html("Removal succeeded");  
           },   
           function(error) {  
          fs.$("#result").html("Error removing file: " + error.code);  
         });  
       },
       removeFail:function(error){
         console.log("FileSystem.gotFSForRemove....remove failed");
        if(error.code==1){
          fs.$("#result").html("File is not exist");
        }
       },


     //Get File Metadata
     getMetadataInfo:function() {  
         window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, this.getFileMetadata, this.fail);  
     },
     getFileMetadata:function(fileSystem){
    	 var me=this;
    	 fileSystem.root.getFile("CordovaSample.txt", {create: false, exclusive: false},
    		function(fileEntry){
        	 fileEntry.getMetadata(
        		function win(metadata) {
        		    me.$("#result").html("File was last modified at "+metadata.modificationTime);
        	 },null);
         }, fs.fail);
     },
     
    //getDirectory
      
    getDirectoryListing: function() {
      console.log("FileSystem.getDirectoryListing...start");
      window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, fs.readerDirectory, fs.fail); 
      console.log("FileSystem.getDirectoryListing...finished");
    },

    readerDirectory:function(fileSystem) {  
    var directoryReader = fileSystem.root.createReader();
        directoryReader.readEntries(fs.getDirSuccess,fs.getDirFail);
        console.log("FileSystem.readerDirectory...................");
    },

    getDirSuccess:function(entries){
      var i,result="";
        for (i=0; i<entries.length; i++) {
            if(entries[i].isFile){
              result+="[F]";
            }else {
              result+="[D]";
      }
            result+="&nbsp;"+entries[i].name+"<br>";
            //result+="&nbsp;"+entries[i].fullPath+"<br>";
        }
        fs.$("#result").html(result);
     },

    getDirFail:function(error) {
         alert("Failed to list directory contents: " + error.code);
     },


    fail: function(error) {  
      alert("error.code:"+error.code);  
    },

    clearLog:function(){
    	this.$("#result").html("");
    },

    goBack: function(){
      window.history.back();
    } 
  });
});
