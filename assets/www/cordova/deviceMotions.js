define(['butterfly/view'], function(View){

  return View.extend({
    events: {
      "click a[data-action='back']": "goBack",
      "click #start": "startWatch",
      "click #end": "stopWatch"
    },
    initialize: function(){
      watchID = null;
      idStr="accelerometerWatchID";
    },
    onShow: function(){
    	console.log(watchID);
    	 var watch_ID=window.sessionStorage[idStr];
         if(watch_ID!=null){
       	  console.log(watch_ID);
       	  watchID=watch_ID;
         }    	
    },
    goBack: function(){
      window.history.back();
    },
    onDeviceReady: function(){
//      this.startWatch;
    },
	startWatch : function() {
		if (watchID == null) {
			var me = this;
			watchID = navigator.accelerometer.watchAcceleration(me.onSuccess, me.onError, {frequency : 1000});
			window.sessionStorage[idStr] = watchID;
		}
	},
    stopWatch: function() {
      if (watchID) {
          navigator.accelerometer.clearWatch(watchID);
          watchID = null;
      }
      window.sessionStorage.removeItem(idStr);
    },
    onSuccess: function(acceleration){
        $("#x").html("x: "+acceleration.x);
        $("#y").html("y: "+acceleration.y);
        $("#z").html("z: "+acceleration.z);
        $("#time").html("Time: "+acceleration.timestamp);
    },
    onError: function() {
        alert('onError!');
    }

  });
});
