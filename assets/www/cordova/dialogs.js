define(['butterfly/view'],function(View){

  return View.extend({
    events: {
      "click a[data-action='back']": "goBack",
      "click #Alert": "notificationAlert",
      "click #Confirm": "notificationConfirm",
      "click #Prompt": "notificationPrompt"
    },
    notificationAlert: function(){
    	navigator.notification.alert(
		    'You are the winner!',  		// message
		     function(){		// callback
		    	console.log(".....");
		    },         
		    'Game Over',            		// title
		    'Done'                  		// buttonName
		);
    },
    notificationConfirm: function(){
    	navigator.notification.confirm(
		    'You are the winner!', 			// message
		     function(buttonIndex) {		// callback to invoke with index of button pressed
			    alert('You selected button ' + buttonIndex);
			},           
		    'Game Over',           			// title
		    ['Restart','Exit']     			// buttonLabels
		);
    },
    notificationPrompt: function(){
		navigator.notification.prompt(
		    'Please enter your name',  		// message
		    function(results){
			    alert("You selected button number " + results.buttonIndex + " and entered " + results.input1);
			},                  			// callback to invoke
		    'Registration',            		// title
		    ['Ok','Exit'],             		// buttonLabels
		    'Jane Doe'                 		// defaultText
		);
    },
    goBack: function(){
      window.history.back();
    }
  });
});
