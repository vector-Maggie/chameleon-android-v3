define(['butterfly/view'],
  function(View,Cordova){

  return View.extend({
    events: {
      "click a[data-action='back']": "goBack",
      "click #splash":"showSplash"
    },
    showSplash:function(){
    	navigator.splashscreen.show();
    	setTimeout(function() {
            navigator.splashscreen.hide();
        }, 500);
    },
    goBack: function(){
      window.history.back();
    }
  });
});
