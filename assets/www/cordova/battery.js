define(['butterfly/view'], function(View){

  return View.extend({
    events: {
      "click a[data-action='back']": "goBack",
      "click .display": "display",
      "click .stop": "stop"
    },
    onShow: function () {
        
        console.log("初始化");
    },

    display:function(){
        alert("Battery status onShow...");        
        window.addEventListener("batterycritical", this.onBatteryCritical, false);
        window.addEventListener("batterylow", this.onBatteryLow, false);
        window.addEventListener("batterystatus", this.onBatteryStatus, false);
    },

    stop: function(){
        alert("Stop watching the battery status...");
        window.removeEventListener("batterycritical", this.onBatteryCritical, false);
        window.removeEventListener("batterylow", this.onBatteryLow, false);
        window.removeEventListener("batterystatus", this.onBatteryStatus, false);
    },

    onBatteryCritical: function(info) {
        alert('Battery level critical, please recharge soon...');
        // Handle the battery critical event
        console.log("Recharge soon!");
         var element=document.getElementById("battery");
        element.innerHTML="Battery Level Critical " + info.level + "%<br>Recharge Soon!";
    },    

    onBatteryLow: function(info) {
        alert("Battery level is lowwer than 20%...");
        // Handle the battery low event
        var element=document.getElementById("battery");
        element.innerHTML="Battery Level Low " + info.level + "%";
    },

    onBatteryStatus: function(info) {
        // Handle the online event
        var element=document.getElementById("battery");
        element.innerHTML="Level: " + info.level + "    isPlugged: " + info.isPlugged;
    },

    goBack: function(){
      window.history.back();
    }
  });
});
    
 
    



    
