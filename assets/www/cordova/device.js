define(['butterfly/view'],
  function(View){

  return View.extend({
    events: {
      "click button#get":"getDeviceInfo",
      "click a[data-action='back']": "goBack"
    },
    initialize:function(){
    	console.log("device.initialize...");
    },
    onShow:function(){
    	document.addEventListener('deviceready', this.onDeviceReady, false);
    	console.log("device.onShow...");
    },
    render:function(){
    	console.log("device.render...");
    },

    onDeviceReady: function() {
        console.log("device.onDeviceReady...");
//        document.getElementById("get").addEventListener("click",this.getDeviceInfo,false);
    },
    getDeviceInfo:function(){
        $("#model").html(device.model);
        $("#cordova").html(device.cordova);
        $("#platform").html(device.platform);
        $("#uuid").html(device.uuid);
        $("#version").html(device.version);
        $("#model").html(device.model);
        $("#cordova").html(device.cordova);
//        $("#name").html(device.name);
        $("#width").html(screen.width);
        $("#height").html(screen.height);
        $("#colorDepth").html(screen.colorDepth);
    },

    goBack: function(){
      window.history.back();
    }
  });
});
