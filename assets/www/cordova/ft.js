define(['butterfly/view'], 
	function(View) {

	return View.extend({
		events : {
			"click a[data-action='back']" : "goBack",
			"click #upload" : "uploadFile",
			"click #download" : "downloadFile",
			"click #abort" : "abortTransfer",
			"click #clear":"clearLog"
		},
		
		fileTransfer : null,
		
		initialize : function() {
			console.log("FileTransfer.initialize...");
		},
		
		render : function() {
			console.log("FileTransfer.render...");
		},
		
		onShow : function() {
			this.fileTransfer = new FileTransfer();
			console.log("FileTransfer.onShow...");
		},
		
		goBack : function() {
			window.history.back();
		},
		
		clearLog:function(){
			$("#result").html("");
		},
		
		uploadFile : function() {
			var me=this;
			console.log("uploadFile...........");
			var success = function (metadata) {
			    console.log("Code = " + metadata.responseCode);
			    console.log("Response = " + metadata.response);
			    console.log("Sent = " + metadata.bytesSent);
			    var result="上传成功!!!";
			    result+="<br/>Code = " + metadata.responseCode;
			    result+="<br/>Response = " + metadata.response;
			    result+="<br/>Sent = " + metadata.bytesSent;
			    $("#result").html(result);
			}

			var fail = function (error) {
			    var result="上传失败……";
			    result+="<br>An error has occurred: Code " + error.code;
			    result+="<br>upload error source " + error.source;
			    result+="<br>upload error target " + error.target;
			    $("#result").html(result);
			}

			var fileURL=cordova.file.applicationDirectory+""+"www/cordova/test.jpg";
			var uploadURL=this.$("#url").val();
			var options = new FileUploadOptions();
			options.fileKey = "file";
			options.fileName = fileURL.substr(fileURL.lastIndexOf('/') + 1);
			options.mimeType = "image/jpeg";
			uploadURL=encodeURI(uploadURL);
			console.log(uploadURL+"...............");
			this.fileTransfer.upload(fileURL,uploadURL,success,fail,options);
		},
		
		downloadFile : function() {
			console.log("downloadFile..............");
			var uri="http://www.baidu.com/img/baidu_sylogo1.gif";
			uri=encodeURI(uri);
			console.log(uri);
			fileName=uri.substring(uri.lastIndexOf("/")+1);
			var fileURL=cordova.file.externalRootDirectory+fileName;
			//var fileURL=cordova.file.applicationDirectory  
			this.fileTransfer.download(
				    uri,
				    fileURL,
				    function(entry) {
				        console.log("download complete: " + entry.toURL());
				        $("#result").html("下载成功！已下载文件路径为：<br/>"+entry.toURL());
				    },
				    function(error) {
				        console.log("download error source " + error.source);
				        console.log("download error target " + error.target);
				        console.log("upload error code" + error.code);
				        $("#result").html("下载失败！<br/>download error source "+ error.source+"<br/>download error target " + error.target+"<br/>upload error code" + error.code);
				    },
				    false,{}
				);
		},
		
		abortTransfer : function() {
			console.log("abortTransfer...........");
			var success = function(r) {
				$("#result").html("Should not be called.");
			}

			var fail = function(error) {
				var result="上传失败……";
			    result+="<br>An error has occurred: Code " + error.code;
			    result+="<br>upload error source " + error.source;
			    result+="<br>upload error target " + error.target;
			    $("#result").html(result);
			}

			var options = new FileUploadOptions();
			options.fileKey="file";
			options.fileName="test.jpg";
			options.mimeType="image/jpeg";
			var fileURL=cordova.file.applicationDirectory+""+"www/cordova/test.jpg";
			var uploadURL=this.$("#url").val();
			this.fileTransfer.upload(fileURL, uploadURL, success, fail, options);
			this.fileTransfer.abort();
		},
	});
});
