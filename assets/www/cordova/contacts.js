define(['butterfly/view'],
  function(View){

  return View.extend({
    events: {
      "click a[data-action='back']": "goBack",
      "click #getContacts":"getContacts"
    },

    initialize:function(){
	    	console.log("contacts.initialize...");
    },

    onShow:function(){
    	console.log("contacts.onShow...");

    },

    render:function(){
    	console.log("contacts.render...");
    },

    goBack: function(){
      window.history.back();
    },


    getContacts:function(){
    	console.log("getContacts..............");
    	var options = new ContactFindOptions();
		options.filter = "";
		options.multiple = true;
		var fields=["phoneNumbers","displayName"];
		// var fields=[navigator.contacts.fieldType.displayName, navigator.contacts.fieldType.name];
    	navigator.contacts.find(fields, this.success, this.fail, options);
    },
    success:function(contacts){
    	alert('Found ' + contacts.length + ' contacts.');
    	var result='Found ' + contacts.length + ' contacts.<br/>';
    	result+="<ul>";
    	// this.$("#detail").html('Found ' + contacts.length + ' contacts.');
    	for (var i = contacts.length - 1; i >= 0; i--) {
    		var displayName = contacts[i].displayName;
    		var phoneNumbers=contacts[i].phoneNumbers;
    		if (contacts[i].phoneNumbers) {
						if (contacts[i].phoneNumbers[0]) {
						} else {
							contacts[i].phoneNumbers[0] = {
								value: ""
							};
						}
					} else {
						contacts[i].phoneNumbers = new Array();
						contacts[i].phoneNumbers[0] = {
							value: ""
						};
					}
			var phoneNum = contacts[i].phoneNumbers[0].value;
    		result+="<li><span class='name'>姓名:"+displayName+ "</span><span class='phone'>电话：" +phoneNum +"</span></li>";
    	};
    	result+="</ul>";
    	this.$("#detail").html(result);
    },
    fail:function(contactError){
    	this.$("#detail").html(contactError);
    }
  });
});
