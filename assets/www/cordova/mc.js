define(['butterfly/view'], 
	function(View) {

	return View.extend({
	    initialize:function(){
	    	console.log("MediaCapture.initialize...");
	    },
	    onShow:function(){
	    	console.log("MediaCapture.onShow...");
	    },
	    render:function(){
	    	console.log("MediaCapture.render...");
	    },
	    
		events : {
			"click a[data-action='back']" : "goBack",
			"click #captureAudio" : "captureAudio",
			"click #captureImage" : "captureImage",
			"click #captureVideo" : "captureVideo",
		},

		goBack : function() {
			window.history.back();
		},
		captureSuccess : function(mediaFiles) {
			var i,len;
			var me=this;
			me.$(".detail").show();
		    for (i = 0, len = mediaFiles.length; i < len; i += 1) {
		        me.$(".name").html(mediaFiles[i].name);
		        me.$(".date").html(new Date(mediaFiles[i].lastModifiedDate));
		        me.$(".size").html(mediaFiles[i].size);
		        me.$(".type").html(mediaFiles[i].type);
		        me.$(".path").html(mediaFiles[i].fullPath);
		    }
			alert("captureSuccess");
		},

		captureError : function(error) {
			 navigator.notification.alert('Error code: ' + error.code, null, 'Capture Error');
		},

		captureAudio : function() {
			console.log("MediaCapture.captureAudio...");
			var options = {
				limit : 1,
				duration : 5
			};
			navigator.device.capture.captureAudio(this.captureSuccess,
					this.captureError, options);
		},

		captureImage : function() {
			console.log("MediaCapture.captureImage...");
			navigator.device.capture.captureImage(this.captureSuccess,
					this.captureError, {
						limit : 1
					});
		},

		captureVideo : function() {
			console.log("MediaCapture.captureVideo...");
			navigator.device.capture.captureVideo(this.captureSuccess,
					this.captureError, {
						limit : 1
					});
		}
	});
});
