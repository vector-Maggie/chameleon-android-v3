define(['butterfly/view',], function(View){

  return View.extend({
    events: {
      "click a[data-action='back']": "goBack",
      "click .get": "getCurrentPosition",
      "click .watch": "watchGeolocation",
      "click .clear": "clearGeolocation"
    },
    watchGeoID: null,

    initialize: function(){
      idStr="geoWatchID";
    },

    onShow: function(){
      console.log("show geolocation");
      var watchID=window.sessionStorage[idStr];
      if(watchID!=null){
        this.watchGeoID=watchID;
        console.log(this.watchGeoID);
      }
    },

    watchGeolocation: function() {
    if(this.watchGeoID==null){
      alert('watchGeolocation');  
      var element = document.getElementById('location');  
      element.innerHTML = 'Finding geolocation every 30 seconds...';
      var options = { enableHighAccuracy: true, timeout: 30000 };  
      this.watchGeoID = navigator.geolocation.watchPosition(this.onGeoSuccess, this.onGeoError, options);  
      window.sessionStorage[idStr]= this.watchGeoID;
    }  
      
    }, 
  
    onGeoSuccess: function(position) {  
      var element = document.getElementById('location');  
      element.innerHTML ='Latitude: ' + position.coords.latitude + '<br />' +  
      'Longitude:  ' + position.coords.longitude + ' <br />' +  
      'Altitude (in meters): ' + position.coords.altitude + ' <br />' +  
      'Accuracy (in meters): ' + position.coords.accuracy + ' <br />' +  
      'Altitude Accuracy: ' + position.coords.altitudeAccuracy + ' <br />' +  
      'Heading (direction of travel): ' + position.coords.heading + ' <br />' +  
      'Speed (meters per second): ' + position.coords.speed + ' <br />' +  
      'Timestamp: ' + position.timestamp + ' <br />';  
    },

    onGeoError: function(error) {  
      var element = document.getElementById('location');  
      if(error.code==error.PERMISSION_DENIED){
        this.element.innerHTML="用户不允许使用位置信息，请在设置中打开定位服务";
      }else if(erro.code==error.POSITION_UNAVAILABLE){
        this.element.innerHTML="无法取得位置信息，可能设备没有GPS功能或没有网络连接";
      }else{
        this.elemnt.innerHTML="指定的时间内没有取得位置信息";
      }
    },  
  
    clearGeolocation: function() {  
      alert('clearGeolocation');
      if(this.watchGeoID!=null){
        var element = document.getElementById('location');  
        element.innerHTML = '<span style="color:red">Geolocation turned off.</span>';   
        navigator.geolocation.clearWatch(this.watchGeoID);  
        this.watchGeoID = null;     
       }  
       window.sessionStorage.removeItem(idStr);
    }, 
  
    getCurrentPosition: function() {  
      alert('getCurrentPosition');
    
         navigator.geolocation.getCurrentPosition(this.onPositionSuccess, this.onPositionError); 
      
    },  

    onPositionSuccess: function(position) {  
       var element = document.getElementById('location');  
      element.innerHTML = 'Latitude: ' + position.coords.latitude + '<br>' +   
      'Longitude: ' + position.coords.longitude + '<br>';  
    },  

    onPositionError: function(error) {  
      var element = document.getElementById('location');  
      element.innerHTML ='Error getting GPS Data';  
    },  

    
    goBack: function(){
      window.history.back();
    }
  });
});
