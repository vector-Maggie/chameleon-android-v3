define(['butterfly/view',
        '../cordova',
    	'css!cordova/style'], 
    	function(View,Cordova){

  return View.extend({
    events: {
      "click a[data-action='back']": "goBack"
    },

    onShow: function(){
      console.log('cordova/index onShow');
    },

    goBack: function(){
      window.history.back();
    }
  });
});
