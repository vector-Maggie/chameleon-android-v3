define(['butterfly/view'], function(View){

  return View.extend({
    events: {
      "click a[data-action='back']": "goBack",
      "click #check-network": "checkConnection",
    },
	checkConnection: function(){
	    var networkState = navigator.connection.type;
	    var states = {};
	    states[Connection.UNKNOWN]  = 'Unknown connection';
	    states[Connection.ETHERNET] = 'Ethernet connection';
	    states[Connection.WIFI]     = 'WiFi connection';
	    states[Connection.CELL_2G]  = 'Cell 2G connection';
	    states[Connection.CELL_3G]  = 'Cell 3G connection';
	    states[Connection.CELL_4G]  = 'Cell 4G connection';
	    states[Connection.CELL]     = 'Cell generic connection';
	    states[Connection.NONE]     = 'No network connection';
	    alert('Connection type: ' + states[networkState]);	   
	},
    goBack: function(){
      window.history.back();
    }
  });
});
