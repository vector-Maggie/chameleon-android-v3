define(['butterfly/view'],function(View) {

	return View.extend({
		events : {
			"click a[data-action='back']" : "goBack",
			"click #openW" : "openWindow",
			"click #closeIn5s" : "closeIn5s",

		},

		goBack : function() {
			window.history.back();
		},

		iabRef : null,

		iabLoadStart : function(event) {
			alert(event.type + ' - ' + event.url);
		},

		iabLoadStop : function(event) {
			alert(event.type + ' - ' + event.url);
		},

		iabLoadError : function(event) {
			alert(event.type + ' - ' + event.message);
		},

		iabClose : function(event) {
			alert(event.type);
			this.iabRef.removeEventListener('loadstart', this.iabLoadStart);
			this.iabRef.removeEventListener('loadstop', this.iabLoadStop);
			this.iabRef.removeEventListener('loaderror', this.iabLoadError);
			this.iabRef.removeEventListener('exit', this.iabClose);
		},

		// device APIs are available
		openWindow : function() {
			this.iabRef = window.open($("#url").val(), '_blank',
					'location=yes');
			this.iabRef.addEventListener('loadstart', this.iabLoadStart);
			this.iabRef.addEventListener('loadstop', this.iabLoadStop);
			this.iabRef.addEventListener('loaderror', this.iabLoadError);
			this.iabRef.addEventListener('exit', this.iabClose);
		},
		closeIn5s : function() {
			var ref = window.open($("#url").val(), '_blank',
					'location=yes');
			setTimeout(function() {
				ref.close();
			}, 5000);
		}
	});
});
