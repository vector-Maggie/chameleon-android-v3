define(['butterfly/view'], function(View){

  return View.extend({
    events: {
      "click a[data-action='back']": "goBack"
    },

    goBack: function(){
      window.history.back();
    }
  });
});
