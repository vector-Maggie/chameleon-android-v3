﻿define(['butterfly/view','jquery','text!schedule/detail.html','css!schedule/detail'],
  function(View,$,Detail){

  return View.extend({
    events:{
      "click .go-back": "goBack", 
    },

    initialize: function(){
    	console.log("初始化");
    
    },
    onShow: function(){
      var me = this;
      console.log("onShow");
      me.getjson();
    },
    goBack: function(){
      window.history.back();
    },
    getjson: function(){
      var me = this;
      $.ajax({ 
        type: "get", 
          url: "../schedule/mock_detail.json", 
          dataType: "json", 
          success: function (data) { 
            // $("#aa").val(data[0].resName); 
            json=JSON.stringify(data);
            me.$(".meeting-title").append(data[1].subject);
            me.$(".meeting-date").append(data[1].begin);
            me.$(".meeting-time").append(data[1].end);
            me.$(".detail-place").append(data[1].place);
            me.$(".person-name").append(data[1].participants);
            me.$(".meeting-attachs .subject").append(data[1].attachs[0].subject);
            me.$(".meeting-attachs .attachs-size").append(data[1].attachs[0].size);
            me.$(".aganda").append(data.aganda);
          }, 
          error: function (XMLHttpRequest, textStatus, errorThrown) { 
            console.log(errorThrown); 
          } 
      });
    }
    
  });
});
