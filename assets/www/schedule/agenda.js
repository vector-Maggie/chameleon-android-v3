define(['butterfly/view','jquery','schedule/swipe','moment','css!schedule/agenda'],
  function(View,$,Swiper,Moment){
	return View.extend({
	    events:{
	      "click a[data-action='back']": "goBack",
	      "click .cal-content": "getDayMeeting",
	      "click .prev": "goPrev",
	      "click .next": "goNext",
	      "click .meeting-info":"goDetail",
	    },
		initialize: function(){
	    	console.log("初始化");
	    },
	    render: function(){
	      	console.log("render");
	      	var me = this;
	      	mounthArr = ["一月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月",];
	      	weekArr = ["周日","周一","周二","周三","周四","周五","周六"];
	      	//先不考虑其他
	      	var date = null;
	      	date = new Date();
  			me.writeDate(date);//本周
			me.prevWeek(date);//前一个星期
			me.nextWeek(date);//后一个星期
			// me.$("#list-content").html("");
			me.setSwipe();//滑动
			me.getjson(0);//加载json
	    },
	    onShow:function(){

	    },
	    //日期滑动
		setSwipe:function() { 
			var me = this;
			var date = new Date();
			var currentDate = null;
			var dateStr = me.$el.find(".cal-day:nth-child(2)").find(".content-day1").attr("id");//获得第二个星期的星期一
			var week = date.getDay();

			if(week===0){
				week = 7;
			}
			var todayStr = moment(date).format("YYYY-MM-DD");	//今天
			if (!document.getElementById(todayStr)) {				
			}else{
				 
			}

			var str = me.$el.find(".cal-day:nth-child(2)").find(".content-day"+week).attr("id");//第二个星期的时间
			
			if(dateStr && todayStr!=str){//判断是否为今天
				currentDate = moment(dateStr)._d;
			}
			if(currentDate===null){
				currentDate = date;
			}

			if(me.mySwipe!=null){
				me.mySwipe.kill();
			}
			me.mySwipe = new Swipe(me.$el.find('#slider')[0], {
			  startSlide: 1,
			  speed: 400,
			  auto: false,
			  continuous: false,
			  disableScroll: false,
			  stopPropagation: false,
			  callback: function(index, elem) {
			  		me.getDetermine(currentDate);
			  },
			  transitionEnd: function(index, elem) {
			  	if(index!=1){//如果滑动不成功,就重新加载数据
			  		me.getDetermine(currentDate); //请求数据，动态判断当前的年月份,并且动态的添加左右不断滑动
			  	}
			  }
			});
		},
	    writeDate: function(currentDate){
	    	var me = this;
	    	var todayWeek = currentDate.getDay();	//获得当天是星期几
	    	var todayDay = currentDate.getDate();	//获得当天是几号
	    	var todayMonth = currentDate.getMonth();//获得月份
	    	var todayYear = currentDate.getFullYear();//年
	    	var days = new Date(todayYear,todayMonth+1,0).getDate();//本月有多少天
	    	var year = null;
			var month = null;
			var day = null;
			
			if(todayWeek===0){
				todayWeek=7;
			}
			var itemId = ".header-day"+todayWeek;
			for(var i=1; i<=7; i++){
				if(todayWeek==7){
					if(moment(currentDate).startOf("week").add("week",-1).add("day",i).format("YYYY-MM-DD") === moment(new Date()).format("YYYY-MM-DD")){	
						me.$el.find(".header-day"+i).text("今天");
					}
				} else{
					if(moment(currentDate).startOf("week").add("day",i).format("YYYY-MM-DD") === moment(new Date()).format("YYYY-MM-DD")){	
						me.$el.find(".header-day"+i).text("今天");
					}
				}
			}
			var currentDay = todayDay-todayWeek;
			if(currentDay<1){
				var currentMonth = todayMonth-1;
				if(currentMonth<1){
				var currentYear = todayYear-1;
				currentMonth = currentMonth+12;
				}
			} else{
				var currentMonth = todayMonth;
				var currentYear = todayYear;
			}
			me.$el.find(".month").text(mounthArr[currentMonth]);	//月份(都是星期一所在的月份)
			me.$el.find(".year").text(currentYear);								//年份(都是星期一所在的年份)

			for(var i = 1; i<8; i++){
				day = todayDay-todayWeek+i;	//日
				var item 	= ".content-day"+i;	//选择元素
				if(day>0 && day<days+1){
					year = todayYear;
					month = todayMonth+1;
				}	else if(day<1){

					//上个月有多少天
					var preDays = new Date(todayYear,todayMonth,0).getDate();
					day = preDays+day;
					month = todayMonth;
					if(month<1){//如果是上一年
						month = todayMonth+12;
						year = todayYear-1;
					} else{
						year = todayYear;
					}
				} else if(day>days){
					day = day-days;
					month = todayMonth+2;
					if(month>12){//如果是下一年
						month = month-12;
						year = todayYear+1;
					} else{
						year = todayYear;
					}
				}
				me.$el.find(item).text(day);
				var dayId = moment(year+"-"+month+"-"+day,"YYYY-MM-DD").format("YYYY-MM-DD");
				me.$el.find(item).attr("id",dayId);//为每个日期数字动态添加id 2014-05-23
			}
		},
		//拼接前一个星期的日期
		prevWeek:function(currentDate) {
			var me = this;
			// me.clearJson();
			me.getjson(1);
			me.getjson(2);

			var todayWeek =	currentDate.getDay();	//获得当天是星期几
			var todayDay = currentDate.getDate();	//获得当天是几号
			var todayMonth = currentDate.getMonth(); //获得月份
			var todayYear = currentDate.getFullYear();	//年
			var days = new Date(todayYear,todayMonth+1,0).getDate();//本月有多少天
			var prevDays = new Date(todayYear,todayMonth,0).getDate();//上个月有多少天
			var year = null;
			var month = null;
			var itemHtml = "<div class='cal-day bar bar-tab'>";
			if(Number(todayWeek)==0){
					todayWeek=7;
				}
			for(var rowNum = 1; rowNum<8; rowNum++){
				var day = todayDay-todayWeek-7+rowNum;//前一个星期的日期
				if(day>=1 && day<=days){//在本月的日期内
					day = day;
					month = todayMonth+1;
					year = todayYear;
				} else if(day+prevDays>=1){ //在上个月日期内
					day = day+prevDays;
					month = todayMonth;
					if(month<1){//如果是上一年
						year = todayYear-1;
						month = todayMonth+12;
					}else{
						year = todayYear;
					}
				}
				var dayId = moment(year+"-"+month+"-"+day,"YYYY-MM-DD").format("YYYY-MM-DD");
				if(rowNum<7){	
					itemHtml += "<div class='tab-item'><div id='"+ dayId +"' class='cal-content content-day"+rowNum+"'>"+day+"</div></div>";
				}else if(rowNum==7){//如果是一星期内的字符串就放在一个div中
					itemHtml += "<div class='tab-item'><div id='"+ dayId +"' class='cal-content content-day"+rowNum+"'>"+day+"</div></div>";
					itemHtml += "</div>";
					me.$el.find(".swipe-wrap").prepend(itemHtml);
					itemHtml = "<div class='cal-day bar bar-tab'>";
				}
			}

		},

		//拼接后一个星期的日期
		nextWeek:function(currentDate) {
			var me = this;
			me.getjson(2);
			var todayWeek =	currentDate.getDay();	//获得当天是星期几
			var todayDay = currentDate.getDate();	//获得当天是几号
			var todayMonth = currentDate.getMonth(); //获得月份
			var todayYear = currentDate.getFullYear();	//年
			var days = new Date(todayYear,todayMonth+1,0).getDate();//本月有多少天
			var nextdays = new Date(todayYear,todayMonth+2,0).getDate();//下个月有多少天
			var year = null;
			var month = null;
			var itemHtml = "<div class='cal-day bar bar-tab'>";
			if(Number(todayWeek)==0){
				todayWeek=7;
			}
			for(var rowNum = 1; rowNum<8; rowNum++){
				var day = todayDay-todayWeek+7+rowNum;//后一个星期的日期
				if(day<=days && day>0){//在本月日期内
					day = day;
					year = todayYear;
					month = todayMonth+1;
				} else if(day-days<=nextdays){ //在下个月日期内
						day = day-days;
						month = todayMonth+2;
						if(month>12){
							year = todayYear+1;
							month = month-12;
						}else{
							year = todayYear;
						}
					}

				var dayId = moment(year+"-"+month+"-"+day,"YYYY-MM-DD").format("YYYY-MM-DD");
				if(rowNum<7){	
						itemHtml += "<div class='tab-item'><div id='"+ dayId +"' class='cal-content content-day"+rowNum+"'>"+day+"</div></div>";
					}else if(rowNum == 7){//如果是一星期内的字符串就放在一个div中
						itemHtml += "<div class='tab-item'><div id='"+ dayId +"' class='cal-content content-day"+rowNum+"'>"+day+"</div></div>";
						itemHtml += "</div>";
						me.$el.find(".swipe-wrap").append(itemHtml);
						itemHtml ="<div class='cal-day bar bar-tab'>"
					}
			}
		},
		//返回上层
	    goBack: function(){
	      window.history.back();
	    },
	    //滑动到下一个星期
		goNext: function() {
			var me = this;
			me.mySwipe.next();
			me.activeDay();
		},

		//滑动到上一个星期
		goPrev: function() {
			var me = this;
			me.mySwipe.prev();
			me.activeDay();	
		},
		//请求数据，动态判断当前的年月份,并且动态的添加左右不断滑动
		getDetermine:function(currentDate) {
			var me = this;
			var index = me.mySwipe.getPos();
			var week = new Date().getDay();
			var todayWeek =	currentDate.getDay();	//获得当天是星期几
			var todayDay = currentDate.getDate();	//获得当天是几号
			var todayMonth = currentDate.getMonth(); //获得月份
			var todayYear = currentDate.getFullYear();	//年
			var days = new Date(todayYear,todayMonth+1,0).getDate();//本月有多少天
		  	var currentDay = null;
		  	var currentMonth = null;
		  	var currentYear = null;
		  	var nextdays = new Date(todayYear,todayMonth+2,0).getDate();//下个月有多少天
				var predays = new Date(todayYear,todayMonth,0).getDate();//上个月有多少天
		  	var weekArr = ["一","二","三","四","五","六","日"];

	  		if(week==0){
	  			week = 7;
	  		}

	  		var itemId = ".header-day"+week;
				var todayStr = moment(new Date()).format("YYYY-MM-DD");
				var length = $(".cal-day").length;
				var weekStr = new Date().getDay();

				if(weekStr == 0){
					weekStr = 7;
				}

		  	if(index==1){


	  		//判断是否为今天
				var str = $(".cal-day:nth-child(2)").children(".tab-item:nth-child("+weekStr+")").children().attr("id");
				if(str==todayStr){
					me.$el.find(itemId).text("今天");
				} else{
					for(var i=1; i<=7; i++){
					itemId =".header-day"+i;
					me.$el.find(itemId).text("星期"+weekArr[i-1]);
					}
					// me.$el.find(itemId).text("星期"+weekArr[week-1]);
				}

  			currentDay = todayDay-todayWeek+1;console.log(currentDay);
  			if(currentDay<1){
  				currentMonth = todayMonth-1;
  				if(currentMonth<0){
  					currentYear = todayYear-1;
  					currentMonth = currentMonth+12;
  				}else{
  					currentYear = todayYear;
  					currentMonth = currentMonth;
  				}
  			} else if(currentDay>days){
  					currentMonth = todayMonth+1;
  					if(currentMonth>11){
  						currentYear = currentYear+1;
  						currentMonth = currentMonth-12;
  					} else{
  						currentYear = todayYear;
  						currentMonth = currentMonth;
  					}
  			} else{
  				currentMonth = todayMonth;
  				currentYear = todayYear;
  			}

				me.$el.find(".month").text(mounthArr[currentMonth]);	//月份(都是星期一所在的月份)
				me.$el.find(".year").text(currentYear);								//年份(都是星期一所在的年份)

				//me.getMeeting(currentDate);	//判断本周是否有会议

  	}	else if(index==0){
  			//删除缓存
  			//window.sessionStorage.removeItem("meetingData");
  			//让小红点消失
  			//me.$el.find(".cal-icon").css("background-color","#fff");

  			if(me.$el.find("#list-content") !== undefined){
						me.$el.find("#list-content").children().remove();
					}

				$(".noData").hide();

  			//判断是否为今天
  			var str = $(".cal-day:nth-child(1)").children(".tab-item:nth-child("+weekStr+")").children().attr("id");
				if(str==todayStr){
					me.$el.find(itemId).text("今天");
				} else{
					for(var i=1; i<=7; i++){
						itemId =".header-day"+i;
						me.$el.find(itemId).text("星期"+weekArr[i-1]);
					}
				} 

  			currentDay = todayDay-todayWeek+(index-1)*7+1;//下个星期的第一天
  			if(currentDay>0){//如果是在本月内
	  				currentMonth = todayMonth;
	  				currentYear = todayYear;
	  			} else if(currentDay<1 && (currentDay+predays)>0){ //如果当前日期在上个月内
	  				currentDay = currentDay+predays;
	  				currentMonth = todayMonth-1;
	  				if(currentMonth<0){//如果是上一年
							currentYear = todayYear-1;
							currentMonth = currentMonth+12;
						}else{
							currentYear = todayYear;
							currentMonth = currentMonth;
						}
	  			}

					me.$el.find(".month").text(mounthArr[currentMonth]);//月份
					me.$el.find(".year").text(currentYear);							//年份
	  
	  			//向前拼接一个星期并删除最后一个星期
	  			var dateStr = me.$el.find(".cal-day:nth-child(1)").find(".cal-content").attr("id");
					var prevjointDate = moment(dateStr)._d;
					me.prevWeek(prevjointDate);
					me.$el.find(".cal-day:nth-child(4)").remove();

					//me.getMeeting(prevjointDate);	//判断本周内哪天有会议

					//初始化swipe
					if($(".cal-day").length==3){
						me.setSwipe();
					}
					

	  		} else if(index==2){

  				if(me.$el.find("#list-content") !== undefined){
						me.$el.find("#list-content").children().remove();
					}

					 $(".noData").hide();
	  			//判断是否为今天

	  			var str = $(".cal-day:nth-child(3)").children(".tab-item:nth-child("+weekStr+")").children().attr("id");
  				if(str==todayStr){
  					me.$el.find(itemId).text("今天");
  				} else{
  					for(var i=1; i<=7; i++){
  						itemId =".header-day"+i;
  						me.$el.find(itemId).text("星期"+weekArr[i-1]);
						}
  				} 

	  			currentDay = todayDay-todayWeek+(index-1)*7+1;//上个星期的第一天
	  			if(currentDay<=days){//如果是在本月内
  				currentMonth = todayMonth;
  				currentYear = todayYear;
  			} else if(currentDay>days && currentDay<=(days+nextdays)){ //如果当前日期在下个月内
  				currentDay = currentDay-days;
  				currentMonth = todayMonth+1;
  				if(currentMonth>11){//如果是下一年
						currentYear = todayYear+1;
						currentMonth = currentMonth-12;
					}else{
						currentYear = todayYear;
						currentMonth = currentMonth;
					}
	  			}
					me.$el.find(".month").text(mounthArr[currentMonth]);//月份
					me.$el.find(".year").text(currentYear);							//年份
	  			
	  			//向后添加一个星期的日期并删除一个星期
	  			var dateStr = me.$el.find(".cal-day:nth-child(3)").find(".cal-content").attr("id");//第三个星期的星期一
					var nextjointDate = moment(dateStr)._d;
					me.nextWeek(nextjointDate); //向后添加一个星期

					me.$el.find(".cal-day:nth-child(1)").remove();//删除第一个星期

					//me.getMeeting(nextjointDate);	//判断本周内哪天有会议

					//初始化swipe
					if($(".cal-day").length==3){
						me.setSwipe();
					}
			
	  		}
			},
	    //获取当天的日程记录
	    getCurrentMeeting: function(el){
	    	var me = this;
	    	var date = null;
	    	date = new Date();	
	    },
	    getDayMeeting: function(el){
	    	//当点击该天时为该天增加一个下滑线
	    	//el参数是事件
			$(".cal-content").removeClass("activeDay");
			$(el.currentTarget).addClass("activeDay");			
	    },
	    //当每次滑动到上下星期时，重新给星期一加下滑线
	    activeDay: function(){
	    	var me = this;
	    	$(".cal-content").removeClass("activeDay");
	    	$(".content-day1").addClass("activeDay");
	    },
	    getjson: function(i){
	    	var me = this;
	    	var $meetingLi = $("<li class='table-view-cell media meeting-info'></li>");//创建会议<li>，从而获取本地json
	    	var $meeting = $('<div class="pull-left meeting">会议</div>');
	    	var $meetingContent = $('<div class="media-body meeting-content">');
	    	var $meetingTime = $("<div class='meeting-time'></div>");
	    	var $meetingTitle = $("<div class='meeting-title'></div>");
	    	var $meetingAddress = $("<div class='meeting-address'></div>");
	    	$.ajax({ 
           			type: "get", 
                    url: "../schedule/mock_list.json", 
                    dataType: "json", 
                    success: function (data) { 
                            json=JSON.stringify(data);		//获得json数据
                            $meetingLi.attr("id",data[i].id);	
                            $meetingTime.append(data[i].begin);
                            $meetingTitle.append(data[i].subject);
                            $meetingAddress.append(data[i].place);
 
                            $meetingContent.append($meetingTime);
                            $meetingContent.append($meetingTitle);
                            $meetingContent.append($meetingAddress);
                            $meetingLi.append($meeting);
                            $meetingLi.append($meetingContent);
                            me.$("#list-content").append($meetingLi);                       
                    }, 
                    error: function (XMLHttpRequest, textStatus, errorThrown) { 
                            console.log(errorThrown); 
                    } 
                    });
	    },
	    //跳转到详细页面
	    goDetail:function(el) {
			var relationId = $(el.currentTarget).attr("id");
			console.log(this);
			if(relationId){
				Backbone.history.navigate('schedule/detail.html',{
						trigger: true
					});

			}
		},
	  });
});
