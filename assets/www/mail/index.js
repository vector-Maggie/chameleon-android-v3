define(['butterfly/view', 'iscroll', 'jquery', 'butterfly/listview/DataSource', 'butterfly/listview/ListView'],
	function(View, IScroll, $, DataSource, ListView) {

		return View.extend({

			id: 'mail_index',
			myScroll: null,
			events: {
				"click .mailModule": "goModel",
				"click .sendMail": "goSendMail",
				"click a[data-action='back']": "goback"
			},

			onShow: function() {
				//write your business logic here :)
				// var me = this;
				// this.getMailCount();
				// this.getMailBoxCap();
				$('title').html('邮件');
				// this.$el.find('#wrapper').height(document.body.clientHeight - 90);//容器层比内容层少40px
				 this.myScroll = new IScroll('#wrapper');

				console.log('clear mail boxes');
				['inbox', 'outbox', 'draftBox', 'wasteBox'].forEach(function(box){
					new DataSource({identifier: 'mail-list-' + box, storage: 'local'}).clear();
					ListView.clear('mail-list-' + box);
				});
			},
			goback: function() {
				window.history.back();
			},
			goModel: function(e) {
				var me = this;
				var tag = $($(e.currentTarget)[0]).find('.moduleName')[0].innerHTML;
				if (tag === "收件箱") {
					Backbone.history.navigate('/mail/mailList.html?listType=inbox', {
						trigger: true
					});
				} else if (tag === "发件箱") {
					Backbone.history.navigate('/mail/mailList.html?listType=outbox', {
						trigger: true
					});
				} else if (tag === "草稿") {
					Backbone.history.navigate('/mail/mailList.html?listType=draftBox', {
						trigger: true
					});
				} else if (tag === "回收站") {
					Backbone.history.navigate('/mail/mailList.html?listType=wasteBox', {
						trigger: true
					});
				}
			},
			goSendMail: function() {
				Backbone.history.navigate('/mail/sendMail.html', {
					trigger: true
				});
			},
			
		}); //view define

	});
