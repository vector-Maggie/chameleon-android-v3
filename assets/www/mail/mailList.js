define(['butterfly/view', 'butterfly/listview/DataSource', 'butterfly/listview/ListView', 'jquery','butterfly/notification'],

function(View,DataSource,ListView,$,Notification,json){
  return View.extend({
  	id: 'mail_list',
  	events: {
      "click a[data-action='back']": "goBack",
      // "click .listview": "onItemSelect"
      "click .sendMail": "goSendMail",
      "click .emailEdit": "onEmailEdit",
	  "click .deleteMail": "deleteMail",
	  "click .moveMail": "moveMail"
    },
    checkArr: [],
    		initialize:function(){
  		 	 console.log("let's go!");

   			 },

			_initListView: function(listType) {
				var me = this;
				var listview = "listview" + listType;
				var data= "datasource" + listType;
				this.datasource = new DataSource({
					storage: 'local',
					identifier: 'mail-list-' + listType,
					url: "../mail/mock_"+listType+".json",
					// pageParam: 'pageIndex',   
					requestParams: {
					// 	mailbox: me.mailboxType,
					// 	onlyUnread: 'false',
					}
					
				});
				var listEl = this.el.querySelector("#mail-list");
				console.log(listEl);
				me.$('ul').html('');
				var template = _.template(this.$("#mail-template").html());
				this.listview = new ListView({
					id: 'mail-list-' + listType,
					el: listEl,
					itemTemplate: template,
					dataSource: this.datasource
				});
				this.listenTo(this.listview, 'itemSelect', this.onItemSelect);
				this.listenTo(this.listview, 'load', this.editShow);

			},
			 getUrlVars : function(){
				var vars = [], hash;
				var href = window.location.href;
				 var hashes = href.slice(href.indexOf('?') + 1).split('&');
				for(var i = 0; i < hashes.length; i++)
    			{
      			hash = hashes[i].split('=');
     			 vars.push(hash[0]);
     			 vars[hash[0]] = hash[1];
  			  	}
    			return vars;
				},

			 	getUrlVar : function(name){
				return this.getUrlVars()[name];
				},

				render: function() {
				var me = this;

				var listType=this.getUrlVar("listType");
				if (listType) {
					switch (listType) {
						case "inbox":
							$('.title').html("收件箱");
							break;
						case "outbox":
							$('.title').html("发件箱");
							break;
						case "draftBox":
							$('.title').html("草稿箱");
							break;
						case "wasteBox":
							$('.title').html("回收站");
							break;
						default:
							$('.title').html("邮件");

					}
					me.mailboxType = listType;
				}
				this._initListView(me.mailboxType);
			},
				editShow:function(){
				this.$el.find('.emailEdit').show();
			},
				onItemSelect: function(listview, item, index, event) {
				var me = this;
				var listType=this.getUrlVar("listType");
				console.log(listType);
				if(listType=="draftBox")
				{
					Backbone.history.navigate('/mail/sendMail.html',{trigger:true});
				}
				else{
				var listID = $($(event.currentTarget).find('.email-model')[0]).attr("data-id");
				console.log(listID);
				var listState = $($(event.currentTarget).find('.email-model')[0]).attr("data-state");
					if (listState === "EDITING") {
						me.listGoSend(listID);
					} else {
						this.getContent(listID);

					}
				}

			},
			getContent:function(listID){
        	var me=this;
        	var listType=this.getUrlVar("listType");
        	console.log(listType);
        	var mailDetail= listType+"_detail";
       		console.log(mailDetail);
        	// console.log("type-->>"+type);
        	if(!window.localStorage[mailDetail]){
          	$.ajax({
            type:"GET",
            url:"../mail/mock_detail.json",
            dataType:"json",
            timeout:5000000,
            success:function(json){
              console.log(json);
              window.localStorage[mailDetail]=JSON.stringify(json);
              console.log(window.localStorage[mailDetail])
            },
            error:function(xhr, status,obj){
              console.log("信息读取失败");
            }
          });
          }
        	Backbone.history.navigate('/mail/mailDetail.html?mailID=' + listID + "&mailType=" + me.mailboxType, {
							trigger: true
						});
    	},
			onEmailEdit: function() {
				var me = this;
				var listType=this.getUrlVar("listType");
				this.listview.setEditing(!this.listview.editing);
				if(this.listview.editing===true){
					me.$el.find('.flex1').hide();
					if(listType==="wasteBox"){
							me.$el.find('.deleteMail').show();
					}else{
							me.$el.find('.editMail').show();
					}
					me.$el.find('.emailEdit').find("span").html("取消");
				}else{
					me.$el.find('.flex1').show();
					me.$el.find('.editMail').hide();
					me.$el.find('.emailEdit').find("span").html("编辑");
				}
			},
			
    		goBack: function(){
      		window.history.back();
      		// $('.title').html("邮件");
   		 },
    		goSendMail: function(){
    		Backbone.history.navigate('/mail/sendMail.html',{trigger:true})
   	 }
  });
});

	