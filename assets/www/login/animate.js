/**
 * Created by wzq on 14-9-11.
 */
var animator = (function () {
    var canvas = document.getElementById('circle_progress_bar');
    var ctx = canvas.getContext('2d');

    //
    var deg2Rad = Math.PI / 180;
    var dAng = 20 * deg2Rad; // 每次滑动的角度
    var timeInerval = 70; //ms
    var bgColor = '#99CC33'; //背景色
    var ftColor = '#ff0000'; //前景色
    var circleWidth = 20.0;
    //
    var currAngle = 0;

    function draw() {
        // clear canvas
        ctx.clearRect(0, 0, canvas.width, canvas.height);

        //
        currAngle += dAng;
        if (currAngle >= Math.PI * 2) {
            currAngle -= Math.PI * 2;
        }
        // console.log('currAngle=' + currAngle);

        // background circle
        ctx.beginPath();
        ctx.lineCap = 'square';
        ctx.lineWidth = circleWidth;
        ctx.strokeStyle = bgColor;
        ctx.arc(120, 120, 100, currAngle, currAngle + dAng, true);
        ctx.stroke();

        // 进度
        ctx.beginPath();
        ctx.strokeStyle = ftColor;
        ctx.lineCap = 'square';
        ctx.lineWidth = circleWidth;
        ctx.arc(120, 120, 100, currAngle, currAngle + dAng, false);
        ctx.stroke();
    }

    var id;

    function start() {
        id = window.setInterval(draw, timeInerval);
    }

    function stop() {
        clearInterval(id);
    }

    return {
        start: start,
        stop: stop
    }
})();
