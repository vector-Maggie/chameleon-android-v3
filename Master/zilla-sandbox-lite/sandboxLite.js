var util = require('util');
var fileEncryptor = require('./lib/fileEncryptor.js');
fileEncryptor = new fileEncryptor();
var fs = require('fs');

process.on('uncaughtException', function(err) {
	console.log(err);
});

/**
 * [fileHelper description] 文件夹递归
 * @param  {[type]} infile_dir [description] 需要加密的文件夹
 * @param  {[type]} outfile_dir [description] 加密后，存放文件的文件夹
 * @key  {[type]} key [description] 加密用的key
 * @return {[type]}          [description]
 */
function fileHelper(infile_dir, outfile_dir, key, callback) {
	var callMyself = arguments.callee;
	if (fs.lstatSync(infile_dir).isDirectory()) { // dir
		try {
			var files = fs.readdirSync(infile_dir);
		} catch (err) {
			console.log('检查资源是否存在？路径是否有问题？%s', err);
			return;
		}
		//新建文件夹
		var exists = fs.existsSync(outfile_dir);
		if (!exists) {
				fs.mkdirSync(outfile_dir);
		}
		files.forEach(function(item, index, array) {
			callMyself(infile_dir + '/' + item, outfile_dir + '/' + item, key, callback); // 递归
		});
	} else { // file
		if (infile_dir.indexOf(".DS_Store") > 0) {
			return;
		}
		fileEncryptor.encrypt(infile_dir, outfile_dir, key, callback);
	}
}

//在命令中获取加密文件夹，加密后放到哪里，密码
var arguments = process.argv.splice(2);

console.log("需要加密的文件夹" + arguments[0]);
console.log("加密后，存放文件的文件夹" + arguments[1]);
console.log("key:" + arguments[2]);

// fileHelper("test","comeontom2","comeontom",null);
fileHelper(arguments[0], arguments[1], arguments[2], function(err) {
	if(err) {
		console.log('\t读取数据出错');
		console.log(err);
	}
});